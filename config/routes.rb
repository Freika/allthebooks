Rails.application.routes.draw do

  devise_for :admins
  mount RailsAdmin::Engine => '/dashboard', as: :rails_admin

  get 'tags/:tags', to: 'books#index', as: :tags
  get 'tags/', to: 'books#index'

  resources :categories, only: [:index, :show], path: '' do
    resources :books, param: :slug, except: [:index, :update, :destroy, :edit], path: '' do
      resources :comments, except: :index
    end
  end

  resources :books, only: [:new, :create]

  root 'categories#index'

  get '/auth/:provider/callback' => 'sessions#create'
  get '/signout' => 'sessions#destroy', as: :signout
  get '/locale/change/:locale', to: 'sessions#change_locale', as: :change_locale
end
