require 'rubygems'
require 'nokogiri'
require 'open-uri'
doc = Nokogiri::HTML(open('http://www.allrubybooks.com'))


books = doc.css('.thumbnail')

File.open("books.txt", "a+") do |f|

  books.each do |book|
    tags =  book.css('.badge').text
    title = book.css('h3').text
    author = book.css('p.author').text
    editor_date = book.css('p.editor-date').text
    link = book.search('a').first['href']

    f.puts "Book.create(tags: '#{tags}', title: '#{title}', author: '#{author}', editor_date: '#{editor_date}', link: '#{link}')"
  end
end
