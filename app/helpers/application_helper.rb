module ApplicationHelper
  def alert_class_for(flash_type)
    {
      success: 'alert-success',
      error: 'alert-danger',
      alert: 'alert-warning',
      notice: 'alert-info'
    }[flash_type.to_sym] || flash_type.to_s
  end

  def remove_tag_from_url(tag)
    current_url = request.original_url
    current_url.gsub("&" << tag.name, '').gsub(tag.name, '').gsub('/&', '/') if current_url.include? tag.name
  end

  def add_tag_to_url(tag)
    current_url = request.original_url
    if current_url.include? "/tags"
      if params[:tags]
        current_url << "&" << tag.name
      else
        current_url << tag.name
      end
    else
      tags_path(tag.name)
    end
  end
end
