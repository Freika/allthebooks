module BooksHelper
  def cyr_to_lat(str)
    str.gsub('а', 'a').gsub('б', 'b').gsub('в', 'v').gsub('г', 'g').gsub('д', 'd').gsub('е', 'e').gsub('ё', 'e').gsub('ж', 'zh').gsub('з', 'z').gsub('и', 'i').gsub('й', 'i').gsub('к', 'k').gsub('л', 'l').gsub('м', 'm').gsub('н', 'n').gsub('о', 'o').gsub('п', 'p').gsub('р', 'r').gsub('с', 's').gsub('т', 't').gsub('у', 'u').gsub('ф', 'f').gsub('х', 'h').gsub('ц', 'c').gsub('ч', 'ch').gsub('ш', 'sh').gsub('щ', 'sh').gsub('ъ', '').gsub('ы', 'i').gsub('ь', '').gsub('э', 'e').gsub('ю', 'yu').gsub('я', 'ya')
  end

  def sort_comments_by(lang)
    comments_ru = []
    comments_en = []
    @book.comments.each do |comment|
      case comment.language
      when "ru"
        comments_ru << comment
      else
        comments_en << comment
      end
    end
    lang == "ru" ? comments_ru : comments_en
  end
end
