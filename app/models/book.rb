class Book < ActiveRecord::Base
  include BooksHelper
  include PgSearch

  acts_as_taggable

  belongs_to :category
  has_many :comments
  has_many :book_translations
  before_create :generate_slug
  validates :title, presence: true, uniqueness: { case_sensitive: false }

  scope :published, ->{ where(published: true).order(created_at: :desc) }

  translates :title, :author, :publisher, :description, :fallbacks_for_empty_translations => true
  pg_search_scope :search_everywhere, associated_against: {book_translations: [:title, :author, :description]}

  def generate_slug
    title = self.title.mb_chars.downcase
    self.slug = cyr_to_lat(title).gsub(/[^a-zA-Z0-9]+/, '-').gsub(/-{2,}/, '-').gsub(/^-|-$/, '')
  end
end
