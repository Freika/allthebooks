class Category < ActiveRecord::Base
  include FriendlyId
  friendly_id :title
  has_many :books
end
