$(document).ready(function(){
    $('.with-tooltip').tooltip();
    $('.tags').readmore({
      startOpen: false,
      maxHeight: 65,
      moreLink: '<a class="text-center" href="#"><i class="fa fa-angle-down fa-2x"></i></a>',
      lessLink: '<a class="text-center" href="#"><i class="fa fa-angle-up fa-2x"></i></a>'
    });
});
