json.extract! @book, :id, :title, :author, :publisher, :year, :link, :description, :language, :created_at, :updated_at
