class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_locale
  helper_method :current_user

  def set_locale
    if cookies[:site_locale] && I18n.available_locales.include?(cookies[:site_locale].to_sym)
      l = cookies[:site_locale].to_sym
    else
      l = I18n.default_locale
      cookies.permanent[:site_locale] = l
    end
    I18n.locale = l
  end

  private

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def not_found
    raise ActionController::RoutingError.new('Routing Error')
  end
end
