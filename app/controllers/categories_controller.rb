class CategoriesController < ApplicationController
  def index
    #refactor
    if params[:search]
      @books = Book.search_everywhere(params[:search])
      if @books.size > 0
        @books
      else
        @books = Book.published
        flash.now[:notice] = "There wasn't found any book."
      end
    end
    @tags = Book.tag_counts_on(:tags)
  end

  def show
    @category = Category.friendly.find(params[:id])
    @books = @category.books
    @tags = @books.tag_counts_on(:tags)
  end
end
