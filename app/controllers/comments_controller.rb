class CommentsController < ApplicationController

  before_action do
    # TODO: Refactor
    unless Rails.env.test?
      redirect_to root_path, notice: 'Пожалуйста, авторизуйтесь через социальные сети.' unless params[:authenticity_token]
    end
  end

  def create
    @book = Book.find_by_slug(params[:book_slug])
    @category = @book.category

    if @book.comments.include? Comment.where(user_id: current_user.id)
      redirect_to category_book_path(@category, @book.slug), notice: 'Вы уже оставили комментарий!'
    end
    @comment = @book.comments.build(comment_params)
    @comment.user_id = current_user.id
    @comment.language = I18n.locale
    if @comment.save
      redirect_to category_book_path(@category, @book.slug), notice: 'Спасибо за комментарий!'
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:book_id, :user_id, :body)
  end

end
