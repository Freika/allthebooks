class BooksController < ApplicationController
  before_action :set_book, only: [:edit, :update, :destroy]
  before_action :redirect_unless_current_user, except: :show

  def index
    @tags = Book.tag_counts_on(:tags)

    if params[:tags]
      @chosen_tags = params[:tags].split('&')
      if @chosen_tags.size > 1
        @books = Book.tagged_with(@chosen_tags, match_all: true)
      else
        @books = Book.tagged_with(@chosen_tags)
      end
    else
      # view only tags if no one tag has been chosen
      # or reditect redirect_to root_path here?
    end
  end

  def show
    @book = Book.find_by_slug(params[:slug]) or not_found
    @book.views += 1
    @book.save

    @related_book = @book.find_related_tags
  end

  def new
    @book = Book.new
  end

  def edit
  end

  def create
    @book = Book.new(book_params)

    if @book.save
      redirect_to root_path, notice: 'Book was successfully created.'
    else
      render :new
    end
  end

  def update
    if @book.update(book_params)
      redirect_to @book, notice: 'Book was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @book.destroy
    redirect_to books_url, notice: 'Book was successfully destroyed.'
  end

  private

  def set_book
    @book = Book.find(params[:id])
  end

  def book_params
    params.require(:book).permit(:title, :author, :publisher, :year, :link, :description, :language, :slug, tag_list: [])
  end

  def redirect_unless_current_user
    if Rails.env.production?
      redirect_to root_path, notice: 'Пожалуйста, войдите через Twitter или Github'
    end
  end
end
