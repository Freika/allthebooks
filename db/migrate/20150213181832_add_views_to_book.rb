class AddViewsToBook < ActiveRecord::Migration
  def change
    add_column :books, :views, :integer, default: 0, null: false
  end
end
