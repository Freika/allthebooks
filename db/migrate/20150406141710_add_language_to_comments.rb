class AddLanguageToComments < ActiveRecord::Migration
  def change
    add_column :comments, :language, :string
  end
end
