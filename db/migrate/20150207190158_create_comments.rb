class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :body
      t.references :book, index: true

      t.timestamps null: false
    end
  end
end
