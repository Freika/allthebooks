class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title
      t.string :author
      t.string :publisher
      t.integer :year
      t.string :link
      t.text :description
      t.string :language

      t.timestamps null: false
    end
  end
end
