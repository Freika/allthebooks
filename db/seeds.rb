admin = Admin.new(
    email: "frey@list.ru",
    password: "00000000",
    password_confirmation: "00000000",
)
admin.save!

Category.create(title: 'ruby')

I18n.locale = :en

book = Book.new(
             title: 'Working with Ruby Threads',
             author: 'Jesse Storimer',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://www.jstorimer.com/products/working-with-ruby-threads',
             description: 'Threading and concurrency are hot topics in the Ruby community.Want to join the conversation? You\'ve probably heard people around you talking mutexes, race conditions, or the GIL. Do you wonder what the heck they\'re talking about? This book is a gentle introduction to these concepts and will give you the knowledge you need to become your teams resident concurrency expert.')
             book.tag_list = 'concurrency'
book.save!

I18n.locale = :ru

book = book.update(
                  title: 'Работа с потоками Руби',
                  author: 'Джесси Стормир',
                  publisher: 'Собственная публикация',
                  description: 'Работа с потоками РубиРабота с потоками РубиРабота с потоками РубиРабота с потоками РубиРабота с потоками РубиРабота с потоками РубиРабота с потоками Руби Работа с потоками РубиРабота с потоками РубиРабота с потоками РубиРабота с потоками РубиРабота с потоками Руби')
I18n.locale = :en


 book = Book.new(
             title: 'Application Testing with Capybara',
             author: 'Matthew Robbins',
             publisher: 'Packt Publishing',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://www.packtpub.com/application-testing-with-capybara/book',
             description: 'This book is for developers and testers who have had some exposure to Ruby but who want to know how to test their applications using Capybara and its compatible drivers, such as Selenium-Webdriver and Rack-Test. The examples are deliberately kept simple and example HTML mark-up is always included so that you can copy the examples to practice and experiment on your own machine.')
             book.tag_list = 'testing'
book.save!

 book = Book.new(
             title: 'What do I test?',
             author: 'Eric Steele',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'https://whatdoitest.com/',
             description: 'What Do I Test?’ will answer that very question so you can stop worrying about tests and start building better Rails applications.')
             book.tag_list = 'testing'
book.save!

 book = Book.new(
             title: 'Build a Ruby Gem',
             author: 'Brandon Hilker',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'http://brandonhilkert.com/books/build-a-ruby-gem/',
             description: 'Whether you\'re an expert Rubyist, or just starting out, this book will guide you through the process of creating your own Ruby gem from start to finish. ')
             book.tag_list = 'gem'
book.save!

 book = Book.new(
             title: 'iOS on Rails',
             author: 'Jessie Young',
             publisher: 'thoughtbot',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://learn.thoughtbot.com/products/25-ios-on-rails-beta',
             description: 'The reference for writing superb iOS apps with Ruby on Rails backends. This book is really two books in one. The the first book covers building a backend API in Rails. We will demonstrate how to create a robust, clean, flexible JSON API. Along the way, we’ll discuss different approaches that we didn’t choose and discuss their drawbacks.The second book walks you through creating an iOS application in Objective-C that uses the Rails API you just created. You’ll learn how the intelligent API choices made in the first section make iOS development far easier.')
             book.tag_list = 'Rails, iOS'
book.save!

 book = Book.new(
             title: 'Ruby Science: The reference for writing fantastic Rails applications',
             author: 'thoughtbot',
             publisher: 'thoughtbot',
             published: true,
             year: 2013,
             language: 'en',
             link: 'https://learn.thoughtbot.com/products/13-ruby-science',
             description: 'The reference for writing fantastic Rails applications. You build web applications, and you love Ruby on Rails because it provides a framework for developing applications that are are fast, fun, and easy to change. Over time these applications can become bloated, development slows down, and changes to the codebase become painful. This book was written to help you learn to detect emerging problems in your codebase; we’ll deliver the solutions for fixing them, and maintaining an application that will be fun to work on for years to come.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Backbone.js on Rails',
             author: 'thoughtbot',
             publisher: 'thoughtbot',
             published: true,
             year: 2012,
             language: 'en',
             link: 'https://learn.thoughtbot.com/products/1-backbone-js-on-rails',
             description: 'Build snappier, more interactive apps with cleaner code and better tests in no time. You build web apps, and you’ve seen the shift towards highly interactive, rich client-side web applications. Over the past years, we’ve seen the same things, and have built our apps, processes, and best practices around this. Learn the better way to build rich, interactive applications using Backbone.js and Rails.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Fearless Refactoring: Rails Controllers',
             author: 'Andrzej Krzywda',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'http://rails-refactoring.com/',
             description: 'Coding an app in Rails is always fun... for the first 3 months.After some time, small, little things show up. The tests are no longer below 1 minute. Thin controllers are not really so thin anymore. Single Responsibility Principle is more of a not-so-funny joke than something that you can apply to your Rails models. The build is taking so long at your machine, that you\'ve almost automated it to immediately open Twitter, gmail, FB, HackerNews because nothing else can be done during the tests. The speed of adding features slows down. It\'s no longer fun to demonstrate new features to the Customer. Your development plan for this release sounds so great. The users could be so happy with the new features. Your code could be so elegantly designed.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Reliably Deploying Rails Applications',
             author: 'Ben Dixon',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/deploying_rails_applications',
             description: 'Mastering Rails application deployment. This book will show you from start to finish how to: Setup a VPS from Scratch; Setup additional servers in minutes; Use Capistrano 3 to deploy quickly and reliably; Automate boring maintenance tasks. If you\'ve got applications on Heroku which are costing you a fortune, this will provide you with the tools you need to move them onto a VPS. This includes running several Rails Applications on a single VPS - great for small side projects. If you\'re already running your app on a VPS but the deploy process is flaky - it sometimes doesn\'t restart or loads the wrong version of the code - this book provides a template for making the process hassle free.')
             book.tag_list = 'deployment'
book.save!

 book = Book.new(
             title: 'Ruby Performance Book',
             author: 'Alexander Dymo',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'http://ruby-performance-book.com/',
             description: 'Part 1. Memory Optimization. Why optimize memory; Ruby memory model; When will GC run; Memory optimization strategies; Step by step guide to memory optimization: tune GC, cycle long running processes, disable GC for memory-intensive operations, force GC at idle time, refactoring to avoid data copying, memory usage in iterators; Ruby on Rails specifics. Part 2. CPU Optimization. Write less Ruby; Iterator-unsafe operations: slow type conversions, slow helpers; Slow metaprogramming. Careful localization. Part 3. Tools. Ruby introspection tools; RubyProf; Valgrind/Massif; Optimization processes: measurements, benchmarking, performance tests.')
             book.tag_list = 'performance'
book.save!

 book = Book.new(
             title: 'Easy Active Record for Rails Developers',
             author: 'W. Jason Gilmore',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://www.easyactiverecord.com/',
             description: 'Easy Active Record for Rails Developers is the ultimate guide to mastering building database-driven Rails applications using the powerful Active Record ORM Framework. By following along with the development of a fun, web application for vintage arcade game aficionados, you’ll learn how to: Choose and implement the belongs_to, has_one, has_many, and has_and_belongs_to_many model associations; Properly validate, filter and manipulate your application’s model data; Manage model schemas using Active Record’s powerful migrations tool; Create and integrate complex web forms; Deftly traverse associations using includes and joins; Monitor, debug and optimize your queries; Integrate user registration and authentication features using the Devise gem.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Confident Ruby',
                author: 'Avdi Grimm',
                publisher: 'Self Published',
             year: 2013,
             language: 'en',
                link: 'http://devblog.avdi.org/2013/08/26/confident-ruby-is-finished/',
             description: 'So what is this book and why should you buy it? Confident Ruby is, first and foremost, a book about joy. It’s about the joy I found when I first discovered how elegantly and succinctly I could state problems in Ruby code. It’s about the joy I gradually lost as the “real world” snuck in and gradually cluttered my code with distracting edge case scenarios, error handling, and checks for nil. And it’s about how I came to recapture that joy, by employing small patterns and stylistic choices to make each method tell a  coherent story.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Crafting Rails 4 Applications: Expert Practices for Everyday Rails Development',
             author: 'José Valim',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://pragprog.com/book/jvrails2/crafting-rails-4-applications',
             description: 'Get ready to see Rails as you’ve never seen it before. Learn how to extend the framework, change its behavior, and replace whole components to bend it to your will. Eight different test-driven tutorials will help you understand Rails’ inner workings and prepare you to tackle complicated projects with solutions that are well-tested, modular, and easy to maintain. This second edition of the bestselling Crafting Rails Applications has been updated to Rails 4 and discusses new topics such as streaming, mountable engines, and thread safety.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Agile Web Development with Rails 4',
             author: 'Sam Ruby',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://pragprog.com/book/rails4/agile-web-development-with-rails-4',
             description: 'Rails just keeps on changing. Both Rails 3 and 4, as well as Ruby 1.9 and 2.0, bring hundreds of improvements, including new APIs and substantial performance enhancements. The fourth edition of this award-winning classic has been reorganized and refocused so it’s more useful than ever before for developers new to Ruby and Rails. Rails 4 introduces a number of user-facing changes, and the book has been updated to match all the latest changes and new best practices in Rails. This includes full support for Ruby 2.0, controller concerns, Russian Doll caching, strong parameters, Turbolinks, new test and bin directory layouts, and much more.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Agile Web Development with Rails 3.2',
             author: 'Sam Ruby',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://pragprog.com/book/rails32/agile-web-development-with-rails-3-2',
             description: 'This edition of this award-winning classic has been reorganized and refocused so it’s more useful than ever before for developers new to Ruby and Rails. Rails 3 was a major release—the changes weren’t just incremental, but structural. So we decided to follow suit. This book isn’t just a mild reworking of the previous edition to make it run with the new Rails. Instead, it’s a complete refactoring. You’ll still find the Depot example at the front, but you’ll also find testing knitted right in. Gone are the long reference chapters—that’s what the web does best. Instead you’ll find more targeted information on all the aspects of Rails you’ll need to be a successful web ')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'RubyMotion',
             author: 'Clay Allsopp',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://pragprog.com/book/carubym/rubymotion',
             description: 'Make beautiful apps with beautiful code: use the elegant and concise Ruby programming language with RubyMotion to write truly native iOS apps with less code while having more fun. You’ll learn the essentials of creating great apps, and by the end of this book, you’ll have built a fully functional API-driven app. Whether you’re a newcomer looking for an alternative to Objective-C or a hardened Rails veteran, RubyMotion allows you to create gorgeous apps with no compromise in performance or developer happiness.')
             book.tag_list = 'iOS'
book.save!

 book = Book.new(
             title: 'Rails Recipes: Rails 3 Edition',
             author: 'Chad Fowler',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://pragprog.com/book/rr2/rails-recipes',
             description: 'Thousands of developers have used the first edition of Rails Recipes to solve the hard problems. Now, five years later, it’s time for the Rails 3 edition of this trusted collection of solutions, completely revised by Rails master Chad Fowler.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'The dRuby Book: Distributed and Parallel Computing with Ruby',
             author: 'Masatoshi Seki (Translated by Makoto Inoue)',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://pragprog.com/book/sidruby/the-druby-book',
             description: 'Learn from legendary Japanese Ruby hacker Masatoshi Seki in this first English-language book on his own Distributed Ruby library. You’ll find out about distributed computing, advanced Ruby concepts and techniques, and the philosophy of the Ruby way—-straight from the source.')
             book.tag_list = 'concurrency'
book.save!

 book = Book.new(
             title: 'Build Awesome Command-Line Applications in Ruby: Control Your Computer, Simplify Your Life',
             author: 'David Bryant Copeland',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://pragprog.com/book/dccar/build-awesome-command-line-applications-in-ruby',
             description: 'Speak directly to your system. With its simple commands, flags, and parameters, a well-formed command-line application is the quickest way to automate a backup, a build, or a deployment and simplify your life. All you’ll need is Ruby, and the ability to install a few gems along the way. Examples written for Ruby 1.9.2, but 1.8.7 should work just as well.')
             book.tag_list = 'scripting'
book.save!

 book = Book.new(
             title: 'Crafting Rails Applications: Expert Practices for Everyday Rails Development',
             author: 'José Valim',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2011,
             language: 'en',
             link: 'http://pragprog.com/book/jvrails/crafting-rails-applications',
             description: 'Rails 3 is a huge step forward. You can now easily extend the framework, change its behavior, and replace whole components to bend it to your will, all without messy hacks. This pioneering book is the first resource that deep dives into the new Rails 3 APIs and shows you how to use them to write better web applications and make your day-to-day work with Rails more productive. Everything covered in this book is valid through at least Rails 3.1 For Rails 4, have a look at the second edition.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Exceptional Ruby: Master the art of handling failure in Ruby',
             author: 'Avdi Grimm',
             publisher: 'Self Published',
             published: true,
             year: 2011,
             language: 'en',
             link: 'http://exceptionalruby.com/',
             description: 'Exceptional Ruby is an in-depth guide to exceptions and failure handling in Ruby. Over 100 pages of content and dozens of working examples cover everything from the mechanics of how exceptions work to how to design a robust failure management architecture for your app or library. Whether you are a Ruby novice or a seasoned veteran, Exceptional Ruby will help you write cleaner, more resilient Ruby code.')
             book.tag_list = 'exceptions'
book.save!

 book = Book.new(
             title: 'Metaprogramming Ruby: Program Like the Ruby Pros',
             author: 'Paolo Perrotta',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2010,
             language: 'en',
             link: 'http://pragprog.com/book/ppmetr/metaprogramming-ruby',
             description: 'As a Ruby programmer, you already know how much fun it is. Now see how to unleash its power, digging under the surface and exploring the language’s most advanced features: a collection of techniques and tricks known as metaprogramming. Once the domain of expert Rubyists, metaprogramming is now accessible to programmers of all levels—from beginner to expert. Metaprogramming Ruby explains metaprogramming concepts in a down-to-earth style and arms you with a practical toolbox that will help you write great Ruby code.')
             book.tag_list = 'metaprogramming'
book.save!

 book = Book.new(
             title: 'Learn to Program (2nd edition)',
             author: 'Chris Pine',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://pragprog.com/book/ltp2/learn-to-program',
             description: 'For this new edition of the best-selling Learn to Program, Chris Pine has taken a good thing and made it even better. First, he used the feedback from hundreds of reader e-mails to update the content and make it even clearer. Second, he updated the examples in the book to use the latest stable version of Ruby, and also to use code that looks more like real-world Ruby code, so that people who have just learned to program will be more familiar with common Ruby techniques. Not only does the Second Edition now include answers to all of the exercises, it includes them twice. First you’ll find the “how you could do it” answers, using the techniques you’ve learned up to that point in the book. Next you’ll see “how Chris Pine would do it”: answers using more advanced Ruby techniques, to whet your appetite as well as providing sort of a “Rosetta Stone” for more elegant solutions.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Programming Cocoa with Ruby: Create
           Compelling Mac Apps Using RubyCocoa',
             author: 'Brian Marick',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://pragprog.com/book/bmrc/programming-cocoa-with-ruby',
             description: 'Programming Cocoa with Ruby brings together two enthusiastic development communities. Ruby programmers will tell you how productive they are with just the right amount of code. Cocoa developers know the importance of a clean, intuitive interface. Now, through Programming Cocoa with Ruby, the joy of Cocoa meets the joy of Ruby.')
             book.tag_list = 'Mac'
book.save!

 book = Book.new(
             title: 'Enterprise Recipes with Ruby and Rails',
             author: 'Maik Schmidt',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://pragprog.com/book/msenr/enterprise-recipes-with-ruby-and-rails',
             description: 'Every web site project is really an enterprise integration project in disguise. Ruby on Rails makes it easier than ever to create complex and good-looking web sites, but there’s plenty of life beyond the rails. The 50+ recipes in this book not only show you how to integrate lurking legacy material using Ruby and Ruby on Rails, but also how to create new and highly functional applications in an enterprise environment. You can build completely new applications by enhancing and combining existing components in creative, compelling new ways.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Rails Test Prescriptions: Keeping Your Application Healthy',
             author: 'Noel Rappin',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2011,
             language: 'en',
             link: 'http://pragprog.com/book/nrtest/rails-test-prescriptions',
             description: 'Rails Test Prescriptions is a comprehensive guide to testing Rails applications, covering Test-Driven Development from both a theoretical perspective (why to test) and from a practical perspective (how to test effectively). It covers the core Rails testing tools and procedures for Rails 2 and Rails 3, and introduces popular add-ons, including RSpec, Shoulda, Cucumber, Factory Girl, and Rcov.')
             book.tag_list = 'Rails, testing'
book.save!

 book = Book.new(
             title: 'FXRuby: Create
           Lean and Mean GUIs with Ruby',
             author: 'Lyle Johnson',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://pragprog.com/book/fxruby/fxruby',
             description: 'FXRuby is one of the most popular libraries for developing graphical user interface applications in Ruby. This book is the quickest and easiest way to get started developing GUI applications using FXRuby. With a combination of tutorial exercises and focused, technical information, this book goes beyond the basics to equip you with proven, practical knowledge and techniques for developing real-world FXRuby applications. Learn directly from the lead developer of FXRuby and you’ll be writing powerful and sophisticated GUIs in your favorite programming language.')
             book.tag_list = 'GUI'
book.save!

 book = Book.new(
             title: 'Scripted GUI Testing with Ruby',
             author: 'Ian Dees',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://pragprog.com/book/idgtr/scripted-gui-testing-with-ruby',
             description: 'If you need to automatically test a user interface, this book is for you. Whether it’s Windows, a Java platform (including Mac, Linux, and others) or a web app, you’ll see how to test it reliably and repeatably. Many automated test frameworks promise the world and deliver nothing but headaches. Fortunately, you’ve got a secret weapon: Ruby. Ruby lets you build up a solution to fit your problem, rather than forcing your problem to fit into someone else’s idea of testing. This book is for people who want to get their hands dirty on examples from the real world—and who know that testing can be a joy when the tools don’t get in the way. It starts with the mechanics of simulating button pushes and keystrokes, and builds up to writing clear code, organizing tests, and beyond.')
             book.tag_list = 'testing, GUI'
book.save!

 book = Book.new(
             title: 'The Rails View: Create
           a Beautiful and Maintainable User Experience',
             author: 'John Athayde, Bruce Williams',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://pragprog.com/book/warv/the-rails-view',
             description: 'Working in the Rails View layer requires a breadth of knowledge and attention to detail unlike anywhere else in Rails. One wrong move can result in brittle, complex views that stop future development in its tracks. Break free from tangles of logic and markup in your views and implement your user interface cleanly and maintainably.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Programming Ruby 1.9 & 2.0 (4th edition): The Pragmatic Programmers\' Guide',
             author: 'Dave Thomas, with Chad Fowler, Andy Hunt',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://pragprog.com/book/ruby4/programming-ruby-1-9-2-0',
             description: 'Ruby is the fastest growing and most exciting dynamic language out there. If you need to get working programs delivered fast, you should add Ruby to your toolbox. This book is the only complete reference for both Ruby 1.9 and Ruby 2.0, the very latest version of Ruby.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'The RSpec Book: Behaviour-Driven Development with RSpec, Cucumber, and Friends',
             author: 'David Chelimsky, Dave Astels, Zach Dennis, Aslak Hellesøy, Bryan Helmkamp, Dan North',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2010,
             language: 'en',
             link: 'http://pragprog.com/book/achbd/the-rspec-book',
             description: 'Behaviour-Driven Development (BDD) gives you the best of Test Driven Development, Domain Driven Design, and Acceptance Test Driven Planning techniques, so you can create better software with self-documenting, executable tests that bring users and developers together with a common language.')
             book.tag_list = 'testing'
book.save!

 book = Book.new(
             title: 'Growing Rails Applications in Practice',
             author: 'Henning Koch, Thomas Eisenbarth',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/growing-rails',
             description: 'Structure large Ruby on Rails apps with the tools you already know and love.Discover a simpler way to scale Rails codebases. Instead of introducing new patterns or service-oriented architecture, we will show how to use discipline, consistency and code organization to make your application grow more gently.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Jump Start Rails',
             author: 'Andy Hawthorne',
             publisher: 'SitePoint',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://www.sitepoint.com/store/jump-start-rails/',
             description: 'In just a few short years, Ruby on Rails has grown from cool, hipster upstart to global powerhouse. Developers around the world are passionate about Rails, and with good reason. BUILD FAST with 60,000+ ready-to-go Ruby gems, powerful, new functionality is a never more than a few keystrokes away. DISCOVER Test-driven development for better programming practices. SCALE APPS Look at GitHub, Twitter, Hulu, and Penny Arcade. All huge. All successful. All Rails. GET HIRED Search any job board, there’s massive demand for Rails developers. Those who have jumped onboard the Ruby on Rails train have never looked back and neither will you. Learn Rails and you’ll write better code, faster, spend less time setting up and maintaining code and more time building. Consider this book an investment in your career. There’s a huge and growing demand for developers with Rails skills, just look at any job board. Learn Rails this weekend.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Instant Sinatra Starter',
             author: 'Joe Yates',
             publisher: 'Packt Publishing',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://www.packtpub.com/sinatra-starter/book',
             description: 'Learn something new in an Instant! A short, fast, focused guide delivering immediate results. Set up a Sinatra project. Deploy your project to the Web. Learn about the advanced features of Sinatra.')
             book.tag_list = 'Sinatra'
book.save!

 book = Book.new(
             title: 'Computer Science Programming Basics in Ruby: Exploring Concepts and Curriculum with Ruby',
             author: 'Ophir Frieder, Gideon Frieder, David Grossman',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://shop.oreilly.com/product/0636920028192.do',
             description: 'If you know basic high-school math, you can quickly learn and apply the core concepts of computer science with this concise, hands-on book. Led by a team of experts, you’ll quickly understand the difference between computer science and computer programming, and you’ll learn how algorithms help you solve computing problems.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Jump Start Sinatra',
             author: 'Darren Jones',
             publisher: 'SitePoint',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://www.sitepoint.com/store/jump-start-sinatra/',
             description: 'Sinatra has become the natural choice for building fast and efficient Ruby applications without the weight of Rails. In fact, consider that the entire Sinatra codebase weighs in at less than 2,000 lines—around 1% the size of Rails! And, unlike Rails, you have the freedom to choose the tools you prefer. So, for instance, you can write database-agnostic code complete with Ajax.')
             book.tag_list = 'Sinatra'
book.save!

 book = Book.new(
             title: 'Ruby and MongoDB Web Development Beginner\'s Guide',
             author: 'Gautam Rege',
             publisher: 'Packt Publishing',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://www.packtpub.com/ruby-and-mongodb-web-development-beginners-guide/book',
             description: 'This book assumes that you are experienced in Ruby development skills - Ruby, HTML, CSS. Having knowledge of using NoSQL will help you get through the concepts quicker, but it is not mandatory. No prior knowledge of MongoDB required. Step-by-step instructions and practical examples to creating web applications with Ruby and MongoDB. Learn to design the object model in a NoSQL way. Create objects in Ruby and map them to MongoDB.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Learning Rails 3: Rails from the Outside In',
             author: 'Simon St. Laurent, Edd Dumbill, Eric J Gruber',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://shop.oreilly.com/product/0636920021322.do',
             description: 'If you’re a web developer or designer ready to learn Rails, this unique book is the ideal way to start. Rather than throw you into the middle of the framework’s Model-View-Controller architecture, Learning Rails 3 works from the outside in. You’ll begin with the foundations of the Web you already know, and learn how to create...')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Exploring Everyday Things with R and Ruby: Discover the world around you through programming',
             author: 'Sau Sheong Chang',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://shop.oreilly.com/product/0636920022626.do',
             description: 'If you’re curious about how things work, this fun and intriguing guide will help you find real answers to everyday problems. By using fundamental math and doing simple programming with the Ruby and R languages, you’ll learn how to model a problem and work toward a solution. All you need is a basic understanding of programming. After a quick introduction to Ruby and R, you’ll explore a wide range of questions by learning how to assemble, process, simulate, and analyze the available data. You’ll learn to see everyday things in a different perspective through simple programs and common sense logic. Once you finish this book, you can begin your own journey of exploration and discovery.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Sinatra: Up and Running: Ruby for the Web, Simply',
             author: 'Alan Harris, Konstantin Haase',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2011,
             language: 'en',
             link: 'http://shop.oreilly.com/product/0636920019664.do',
             description: '')
             book.tag_list = 'Sinatra'
book.save!

 book = Book.new(
             title: 'The Book of Ruby: A Hands-On Guide for the Adventurous',
             author: 'Huw Collingbourne',
             publisher: 'No Starch Press',
             published: true,
             year: 2011,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9781593272944.do',
             description: 'Take advantage of Sinatra, the Ruby-based web application library and domain-specific language used by Heroku, GitHub, Apple, Engine Yard, and other prominent organizations. With this concise book, you will quickly gain working knowledge of Sinatra and its minimalist approach to building both standalone and modular web applications. Sinatra serves as a lightweight wrapper around Rack middleware, with syntax that maps closely to functions exposed by HTTP verbs, which makes it ideal for web services and APIs. If you have experience building applications with Ruby, you’ll quickly learn language fundamentals and see under-the-hood techniques, with the help of several practical examples. Then you’ll get hands-on experience with Sinatra by building your own blog engine.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Ruby on Rails For Dummies',
             author: 'Barry Burd',
             publisher: 'Wiley / For Dummies',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.dummies.com/store/product/productCd-0470134895.html',
             description: '')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Cloning Internet Applications with Ruby',
             author: 'Chang Sau Sheong',
             publisher: 'Packt Publishing',
             published: true,
             year: 2010,
             language: 'en',
             link: 'http://www.packtpub.com/cloning-internet-applications-with-ruby/book',
             description: 'Build your own custom social networking, URL shortening, and photo sharing websites using Ruby. Deploy and launch your custom high-end web applications. Learn what makes popular social networking sites such as Twitter and Facebook tick. Understand features of some of the most famous photo sharing and social networking websites. A fast-paced tutorial to get you up and running with cloning some of the most impressive applications available on the Web. This book is written for web application programmers with an intermediate knowledge of Ruby. You should also know how web applications work and you have used at least some of the cloned Internet services before. If you are a trying to find out exactly how can you make your very own customized applications such as TinyURL, Twitter, Flickr, or Facebook, this book is for you. Programmers who want to include features of these Internet services into their own web applications will also find this book interesting.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Pro Active Record',
             author: 'Chad Pytel , Jon Yurek , Kevin Marshall',
             publisher: 'apress',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.apress.com/9781590598474',
             description: 'Pro Active Record helps you take advantage of the full power of your database engine from within your Ruby programs and Rails applications. ActiveRecord, part of the magic that makes the Rails framework so powerful and easy to use, is the model element of the Rails Model-View-Controller framework. It\'s an object-relational mapping library enabling you to interact with databases from both Ruby and Rails applications. Because ActiveRecord is configured with default assumptions that mesh perfectly with the Rails framework, Rails developers often find they hardly need think about it at all. However, if you are developing in Ruby without Rails, or are deploying against legacy databases designed without Rails in mind, or you just want to take advantage of database-specific features such as large objects and stored procedures, you need the in-depth knowledge of ActiveRecord found in this book. In Pro Active Record, authors Kevin Marshall, Chad Pytel, and Jon Yurek walk you through every step from the basics of getting and installing the ActiveRecord library to working with legacy schema to using features specific to each of todays most popular database engines, including Oracle, MS SQL, MySQL, and more! You’ll come to a deep understanding of ActiveRecord that will enable you to truly exploit all that Ruby, Rails, and your chosen database platform have to offer.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Ruby Best Practices: Increase Your Productivity - Write Better Code',
             author: 'Gregory T Brown',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596523015.do',
             description: 'How do you write truly elegant code with Ruby? Ruby Best Practices is for programmers who want to use Ruby as experienced Rubyists do. Written by the developer of the Ruby project Prawn, this concise book explains how to design beautiful APIs and domain-specific languages with Ruby, as well as how to work with functional programming ideas and techniques that can simplify your code and make you more productive. You\'ll learn how to write code that\'s readable, expressive, and much more. Ruby Best Practices will help you: Understand the secret powers unlocked by Ruby\'s code blocks; Learn how to bend Ruby code without breaking it, such as mixing in modules on the fly; Discover the ins and outs of testing and debugging, and how to design for testability; Learn to write faster code by keeping things simple; Develop strategies for text processing and file management, including regular expressions; Understand how and why things can go wrong; Reduce cultural barriers by leveraging Ruby\'s multilingual capabilities. This book also offers you comprehensive chapters on driving code through tests, designing APIs, and project maintenance. Learn how to make the most of this rich, beautiful language with Ruby Best Practices.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Head First Rails: A learner\'s companion to Ruby on Rails',
             author: 'David Griffiths',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596515775.do',
             description: '')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'JRuby Cookbook',
             author: 'Justin Edelson, Henry Liu',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596519650.do',
             description: 'Ready to transport your web applications into the Web 2.0 era? Head First Rails takes your programming -- and productivity -- to the max. You\'ll learn everything from the fundamentals of Rails scaffolding to building customized interactive web apps using Rails\' rich set of tools and the MVC framework. Please note this book covers Rails 2. By the time you\'re finished, you\'ll have learned more than just another web framework. You\'ll master database interactions, integration with Ajax and XML, rich content, and even dynamic graphing of your data -- all in a fraction of the time it takes to build the same apps with Java, PHP, ASP.NET, or Perl. You\'ll even get comfortable and familiar with Ruby, the language that underpins Rails. But you\'ll do it in the context of web programming, and not through boring exercises such as "Hello, World!" Your time is way too valuable to waste struggling with new concepts. Using the latest research in cognitive science and learning theory to craft a multi-sensory learning experience, Head First Rails uses a visually rich format designed to take advantage of the way your brain really works.')
             book.tag_list = 'JRuby'
book.save!

 book = Book.new(
             title: 'Enterprise Rails',
             author: 'Dan Chak',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596515201.do',
             description: 'What does it take to develop an enterprise application with Rails? Enterprise Rails introduces several time-tested software engineering principles to prepare you for the challenge of building a high-performance, scalable website with global reach. You\'ll learn how to design a solid architecture that ties the many parts of an enterprise website together, including the database, your servers and clients, and other services as well. Many Rails developers think that planning for scale is unnecessary. But there\'s nothing worse than an application that fails because it can\'t handle sudden success. Throughout this book, you\'ll work on an example enterprise project to learn first-hand what\'s involved in architecting serious web applications. With this book, you will: Tour an ideal enterprise systems layout: how Rails fits in, and which elements don\'t rely on Rails; Learn to structure a Rails 2.0 application for complex websites; Discover how plugins can support reusable code and improve application clarity; Build a solid data model -- a fortress -- that protects your data from corruption; Base an ActiveRecord model on a database view, and build support for multiple table inheritance; Explore service-oriented architecture and web services with XML-RPC and REST; See how caching can be a dependable way to improve performance; Building for scale requires more work up front, but you\'ll have a flexible website that can be extended easily when your needs change. Enterprise Rails teaches you how to architect scalable Rails applications from the ground up. "Enterprise Rails is indispensable for anyone planning to build enterprise web services. It\'s one thing to get your service off the ground with a framework like Rails, but quite another to construct a system that will hold up at enterprise scale. The secret is to make good architectural choices from the beginning. Chak shows you how to make those choices. Ignore his advice at your peril."-- Hal Abelson, Prof. of Computer Science and Engineering, MIT.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Rails: Up and Running, 2nd Edition: Lightning-Fast Web Development',
             author: 'Bruce A. Tate, Lance Carlson, Curt Hibbs',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596522018.do',
             description: 'In just a matter of days, you can develop powerful web applications with Rails that once took weeks or months to produce with other web frameworks. If that sounds too good to be true, it isn\'t. Find out for yourself with Rails: Up and Running, the concise and popular book that not only explains how Rails works, but guides you through a complete test drive. Perfect for beginning web developers, this thoroughly revised edition teaches you the basics of installing and using Rails 2.1 and the Ruby scripting language. While Rails is praised for its simplicity, there are still a few tricky steps to master along the way. Rails: Up and Running offers lots of examples and covers just about everything you need to build functional Rails applications right away. ')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Rails Pocket Reference: A Quick Guide to Rails',
             author: 'Eric Berry',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596520717.do',
             description: 'Rails 2.1 brings a new level of stability and power to this acclaimed web development framework, but keeping track of its numerous moving parts is still a chore. Rails Pocket Reference offers you a painless alternative to hunting for resources online, with brief yet thorough explanations of the most frequently used methods and structures supported by Rails 2.1, along with key concepts you need to work through the framework\'s most tangled corners. Organized to help you quickly find what you need, this book will not only get you up to speed on how Rails works, it also provides a handy reference you can use anywhere, anytime. ')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'The Art of Rails',
             author: 'Edward Benson',
             publisher: 'Wiley / Wrox',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780470189481.do',
             description: 'Any programmer knows that an API is only half the story, and with Ruby on Rails® this is especially true. Good Rails development, like good web development, is much more about the design choices you make than the framework you have at your disposal. Written by an experienced web application developer, this book picks up where the API leaves off and explains how to take good Rails code and turn it into beautiful Rails code: simple, effective, reusable, evolvable code. In a blend of design and programming, this book identifies and describes the very latest in design patterns, programming abstractions, and development methodologies that have emerged for the modern web. Ruby on Rails offers a completely new way of thinking about and using these emerging techniques, and learning to think like a Rails developer will enable you to rapidly design and write high-quality code with elegance and maintainability. With each design technique, you will discover how Rails incorporates it into its domain-specific language, and you will learn how to weave it seamlessly into your own Rails applications. Then you will learn how to take the next step and transform yourself from user into creator, making your own additions to Rails and crafting a development environment tailored to your specific needs. ')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Aptana RadRails: An IDE for Rails Development',
             author: 'Javier Ramirez',
             publisher: 'Packt Publishing',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9781847193988.do',
             description: 'Aptana RadRails is an Integrated Development Environment for Ruby on Rails projects. Built on top of the well-known Eclipse platform, RadRails provides all the tools you need to create a whole Rails application from a single interface, allowing you to focus on the creative part of the development as your IDE takes care of the mechanics.')
             book.tag_list = 'IDE'
book.save!

 book = Book.new(
             title: 'Professional Ruby on Rails',
             author: 'Noel Rappin',
             publisher: 'Wiley / Wrox',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780470223888.do',
             description: 'Nothing less than a revolution in the way web applications are constructed,Ruby on Rails (RoR) boasts a straightforward and intuitive nature that avoids programming repetition and makes it infinitely easier to build for the web. Over the years, RoR has undergone numerous internal changes while programming techniques have evolved. This book captures the current best practices to show you the most efficient way to build a spectacular web application with RoR. Preparing you to build a real, live, and complex web application, this invaluable resource covers all the questions that aren\'t addressed in introductory material, including how to manage users and security, organize a team of programmers, automate common build tasks, secure your site, and deploy a production server. You\'ll learn everything you need to know in order to extend Rails so that you can take advantage of the many exciting and wonderful things that are being done by the diligent RoR programming community.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Ruby on Rails Web Mashup Projects',
             author: 'Chang Sau Sheong',
             publisher: 'Packt Publishing',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9781847193933.do',
             description: 'A web mashup is a new type of web application that uses data and services from one or more external sources to build entirely new and different web applications. Web mashups usually mash up data and services that are available on the Internet, freely, commercially, or through other partnership agreements. The external sources that a mashup uses are known as mashup APIs. This book shows you how to write web mashups using Ruby on Rails, the new web application development framework. The book has seven real-world projects and each project is described in a methodical step-by-step way, showing how you can write a web mashup from the ground up.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Building Dynamic Web 2.0 Websites with Ruby on Rails',
             author: 'A P Rajshekhar',
             publisher: 'Packt Publishing',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9781847193414.do',
             description: 'Ruby on Rails is an open-source web application framework ideally suited to building business applications; accelerating and simplifying the creation of database-driven websites. Often shortened to Rails or RoR, it provides a stack of tools to rapidly build web applications based on the Model-View-Controller design pattern. This book is a tutorial for creating a complete website with Ruby on Rails. It will teach you to develop database-backed web applications according to the Model-View-Controller pattern. It will take you on a joy ride right from installation to a complete dynamic website. All the applications discussed in this book will help you add exciting features to your website. This book will show you how to assemble RoR\'s features and leverage its power to design, develop, and deploy a fully featured website. Each chapter adds a new feature to the site, adding new knowledge, skills, and techniques. Learn to create dynamic websites with Ruby on Rails.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'The Ruby Programming Language',
             author: 'David Flanagan, Yukihiro Matsumoto',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596516178.do',
             description: ' The Ruby Programming Language is the authoritative guide to Ruby and provides comprehensive coverage of versions 1.8 and 1.9 of the language. It was written (and illustrated!) by an all-star team: David Flanagan, bestselling author of programming language "bibles" (including JavaScript: The Definitive Guide and Java in a Nutshell) and committer to the Ruby Subversion repository. Yukihiro "Matz" Matsumoto, creator, designer and lead developer of Ruby and author of Ruby in a Nutshell, which has been expanded and revised to become this book. why the lucky stiff, artist and Ruby programmer extraordinaire. This book begins with a quick-start tutorial to the language, and then explains the language in detail from the bottom up: from lexical and syntactic structure to datatypes to expressions and statements and on through methods, blocks, lambdas, closures, classes and modules. The book also includes a long and thorough introduction to the rich API of the Ruby platform, demonstrating -- with heavily-commented example code -- Ruby\'s facilities for text processing, numeric manipulation, collections, input/output, networking, and concurrency. An entire chapter is devoted to Ruby\'s metaprogramming capabilities. The Ruby Programming Language documents the Ruby language definitively but without the formality of a language specification. It is written for experienced programmers who are new to Ruby, and for current Ruby programmers who want to challenge their understanding and increase their mastery of the language.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Rails on Windows',
             author: 'Curt Hibbs, Brian P. Hogan',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596529659.do',
             description: 'It\'s no secret that the entire Ruby onRails core team uses OS X as their preferreddevelopment environment. Becauseof this, it is very easy to findauthoritative information on the webabout using Rails on OS X. But the truthis that Windows developers using Railsprobably outnumber those using otherplatforms. A Windows development environmentcan be just as productive asany other platform. This is a guide to developing with Rubyon Rails under Windows. It won\'t teachyou how to write Ruby on Rails web applications,but it will show you what toolsto use and how to set them up to createa complete Rails development environment.')
             book.tag_list = 'Windows'
book.save!

 book = Book.new(
             title: 'Enterprise Integration with Ruby',
             author: 'Maik Schmidt',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://pragprog.com/book/fr_eir/enterprise-integration-with-ruby',
             description: 'Learn how the power and elegance of Ruby can make it easier to get enterprise-level applications to work together. See how Ruby can be used as the glue to combine applications in new ways, extending their lives and increasing their (and your) value to your company.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Ruby on Rails for Microsoft Developers',
             author: 'Antonio Cangiano',
             publisher: 'Wiley / Wrox',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780470374955.do',
             description: 'With its valuable combination of simplicity and productivity, Ruby on Rails is a portable, extendable, open source web application framework that quickly gained worldwide acclaim. The aim of this book is to make the enticing Rails methodology more approachable for developers who are already familiar with Microsoft tools. After an overview and brief history of the framework, you\'ll focus on learning Rails development from the point of view of a beginner-to-intermediate level Microsoft developer. The author explores all the fundamental aspects of Rails, and includes comparisons and references to Microsoft development tools that you may already be familiar with. In doing so, he provides you with an easier path to learn how Rails simplifies the design and implementation of web applications. By serving as a roadmap for migrating your skill set, development processes, and applications to the newer Agile programming platform that Rails offers, this book will help you leverage your existing skills so you can quickly take advantage of the full potential of Rails.')
             book.tag_list = 'Windows'
book.save!

 book = Book.new(
             title: 'Wicked Cool Ruby Scripts: Useful Scripts that Solve Difficult Problems',
             author: 'Steve Pugh',
             publisher: 'No Starch Press',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9781593271824.do',
             description: ' Wicked Cool Ruby Scripts is a compendium of scripts that will make your life easier by saving you time and solving problems. If you find yourself wasting effort and time on a task a trained monkey could do, it\'s time for Ruby to come to the rescue. Wicked Cool Ruby Scripts provides carefully selected Ruby scripts that are immediately useful. Learn how to streamline administrative tasks like renaming files, disabling processes, and changing permissions. After you get your feet wet creating basic scripts, author Steve Pugh will show you how to create powerful Web crawlers, security scripts, full-fledged libraries and applications, and much more. With each script you\'ll get the raw code followed by an explanation of how it really works, as well as instructions for how to run the script and suggestions for customizing it. Wicked Cool Ruby Scripts will save you from the tedium of repetitive tasks and give you back the time it would take to write scripts from scratch.')
             book.tag_list = 'scripting'
book.save!

 book = Book.new(
             title: 'Best of Ruby Quiz',
             author: 'James Edward Gray II',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://pragprog.com/book/fr_quiz/best-of-ruby-quiz',
             description: 'Sharpen your Ruby programming skills with twenty-five challenging problems from Ruby Quiz. Read the problems, work out a solution, and compare your solution with others. This book is a collection of highlights from the first year of Ruby Quiz challenges. Inside, you will find expanded content, all new solutions, and more in depth discussions of Ruby Quiz problems and solutions. This is the book for anyone who really wants to improve their Ruby skills.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Rails for Java Developers',
             author: 'Stuart Halloway, Justin Gehtland',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://pragprog.com/book/fr_r4j/rails-for-java-developers',
             description: 'Many Java developers are now looking at Ruby, and the Ruby on Rails web framework. If you are one of them, this book is your guide. Written by experienced developers who love both Java and Ruby, this book will show you, via detailed comparisons and commentary, how to translate your hard-earned Java knowledge and skills into the world of Ruby and Rails.')
             book.tag_list = 'Java'
book.save!

 book = Book.new(
             title: 'Scripting Intelligence',
             author: 'Mark Watson',
             publisher: 'apress',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://www.apress.com/9781430223511',
             description: 'While Web 2.0 was about data, Web 3.0 is about knowledge and information. Scripting Intelligence: Web 3.0 Information Gathering and Processing offers the reader Ruby scripts for intelligent information management in a Web 3.0 environment—including information extraction from text, using Semantic Web technologies, information gathering (relational database metadata, web scraping, Wikipedia, Freebase), combining information from multiple sources, and strategies for publishing processed information. This book will be a valuable tool for anyone needing to gather, process, and publish web or database information across the modern web environment.')
             book.tag_list = 'scripting'
book.save!

 book = Book.new(
             title: 'Advanced Rails',
             author: 'Brad Ediger',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596510329.do',
             description: 'Ready to go to the next level with Rails? From examining the parts of Ruby that make this framework possible to deploying large Rails applications, Advanced Rails offers you an in-depth look at techniques for dealing with databases, security, performance, web services and much more. Chapters in this book help you understand not only the tricks and techniques used within the Rails framework itself, but also how to make use of ideas borrowed from other programming paradigms. Advanced Rails pays particular attention to building applications that scale -- whether "scale" means handling more users, or working with a bigger and more complex database. You\'ll find plenty of examples and code samples that explain: Aspects of Ruby that are often confusing or misunderstood; Metaprogramming; How to develop Rails plug-ins; Different database management systems; Advanced database features, including triggers, rules, and stored procedures; How to connect to multiple databases; When to use the Active Support library for generic, reusable functions; Security principles for web application design, and security issues endemic to the Web; When and when not to optimize performance; Why version control and issue tracking systems are essential to any large or long-lived Rails project. Advanced Rails also gives you a look at REST for developing web services, ways to incorporate and extend Rails, how to use internationalization, and many other topics. If you\'re just starting out with rails, or merely experimenting with the framework, this book is not for you. But if you want to improve your skills with Rails through advanced techniques, this book is essential.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Ruby Pocket Reference',
             author: 'Michael Fitzgerald',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596514815.do',
             description: 'Although Ruby is an easy language to learn, in the heat of action you may find that you can\'t remember the correct syntax for a conditional or the name of a method. This handy pocket reference offers brief yet clear explanations of Ruby\'s core components, from operators to reserved words to data structures to method syntax, highlighting those key features that you\'ll likely use every day when coding Ruby.Whether you\'ve come to Ruby because of the Rails web development framework --Ruby\'s killer app -- or simply because it\'s a relatively clean, powerful and expressive language that\'s useful for a lot of applications, the Ruby Pocket Reference is organized to help you find what you need quickly. This book not only will get you up to speed on how Ruby works, it provides you with a handy reference you can use anywhere, anytime.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Ruby by Example: Concepts and Code',
             author: 'Kevin C. Baird',
             publisher: 'No Starch Press',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9781593271480.do',
             description: 'There may be no better way to learn how to program than by dissecting real, representative examples written in your language of choice. Ruby by Example analyzes a series of Ruby scripts, examining how the code works, explaining the concepts it illustrates, and showing how to modify it to suit your needs. Baird\'s examples demonstrate key features of the language (such as inheritance, encapsulation, higher-order functions, and recursion), while simultaneously solving difficult problems (such as validating XML, creating a bilingual program, and creating command-line interfaces). Each chapter builds upon the previous, and each key concept is highlighted in the margin to make it easier for you to navigate the book.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Learning Ruby: The Language that Powers Rails',
             author: 'Michael Fitzgerald',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596529864.do',
             description: 'You don\'t have to know everything about a car to drive one, and you don\'t need to know everything about Ruby to start programming with it. Written for both experienced and new programmers alike, Learning Ruby is a just-get-in-and-drive book -- a hands-on tutorial that offers lots of Ruby programs and lets you know how and why they work, just enough to get you rolling down the road. Interest in Ruby stems from the popularity of Rails, the web development framework that\'s attracting new devotees and refugees from Java and PHP. But there are plenty of other uses for this versatile language. The best way to learn is to just try the code! You\'ll find examples on nearly every page of this book that you can imitate and hack. Briefly, this book: Outlines many of the most important features of Ruby; Demonstrates how to use conditionals, and how to manipulate strings in Ruby. Includes a section on regular expressions; Describes how to use operators, basic math, functions from the Math module, rational numbers, etc. Talks you through Ruby arrays, and demonstrates hashes in detail; Explains how to process files with Ruby; Discusses Ruby classes and modules (mixins) in detail, including a brief introduction to object-oriented programming (OOP); Introduces processing XML, the Tk toolkit, RubyGems, reflection, RDoc, embedded Ruby, metaprogramming, exception handling, and other topics; Acquaints you with some of the essentials of Rails, and includes a short Rails tutorial. Each chapter concludes with a set of review questions, and appendices provide you with a glossary of terms related to Ruby programming, plus reference material from the book in one convenient location. If you want to take Ruby out for a drive, Learning Ruby holds the keys.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Beginning Ruby on Rails',
             author: 'Steve Holzner',
             publisher: 'Wiley / Wrox',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780470069158.do',
             description: 'Ruby on Rails is the revolutionary online programming tool thatmakes creating functional e-commerce web sites faster and easierthan ever. With the intuitive, straightforward nature of Ruby andthe development platform provided by Rails, you can put togetherfull-fledged web applications quickly, even if you\'re new to webprogramming. You will find a thorough introduction to both Ruby and Rails inthis book. You\'ll get the easy instructions for acquiring andinstalling both; understand the nature of conditionals, loops,methods, and blocks; and become familiar with Ruby\'s classes andobjects. You\'ll learn to build Rails applications, connect todatabases, perform necessary testing, and put the whole thingtogether to create real-world applications such as shopping cartsand online catalogs—apps you can actually use right away.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Rails Cookbook: Recipes for Rapid Web Development with Ruby',
             author: 'Rob Orsini',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596527310.do',
             description: ' Rails Cookbook is packed with the solutions you need to be a proficient developer with Rails, the leading framework for building the new generation of Web 2.0 applications. Recipes range from the basics, like installing Rails and setting up your development environment, to the latest techniques, such as developing RESTful web services. With applications that are code light, feature-full and built to scale quickly, Rails has revolutionized web development. The Rails Cookbook addresses scores of real-world challenges; each one includes a tested solution, plus a discussion of how and why it works, so that you can adapt the techniques to similar situations. Topics include: Modeling data with the ActiveRecord library; Setting up views with ActionView and RHTML templates; Building your application\'s logic into ActionController; Testing and debugging your Rails application; Building responsive web applications using JavaScript and Ajax; Ensuring that your application is security and performs well; Deploying your application with Mongrel and Apache; Using Capistrano to automate deployment; Using the many Rails plugins; Working with graphics; Whether you\'re new to Rails or an experienced developer, you\'ll discover ways to test, debug and secure your applications, incorporate Ajax, use caching to improve performance, and put your application into production. Want to get ahead of the Web 2.0 curve? This valuable cookbook will save you hundreds of hours when developing applications with Rails.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Capistrano and the Rails Application Lifecycle',
             author: 'Tom Mornini, Marc Loy',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596529628.do',
             description: 'Learn how to make your Rails deployments pain-free with Capistrano! This Short Cut shows you how to use Capistrano to automate the deployment of your Rails applications. It teaches you the basics, but also goes far beyond. It shows you realistic deployment scenarios, including some with complex server farms. It includes a quick reference to Capistrano. As your Rails applications grow, it becomes increasingly important to automate deployment and to keep your development environment well organized. Capistrano is the right tool for the job, and this PDF shows you how to use it effectively.')
             book.tag_list = 'deployment'
book.save!

 book = Book.new(
             title: 'Ruby on Rails: Up and Running: Lightning Fast Web Development',
             author: 'Bruce A. Tate, Curt Hibbs',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596101329.do',
             description: 'Ruby on Rails is the super-productive new way to develop full-featuredweb applications. With Ruby on Rails, powerful web applications thatonce took weeks or months to develop can now be produced in a matter ofdays. If it sounds too good to be true, it isn\'t. If you\'re like a lot of web developers, you\'ve probably consideredkicking the tires on Rails - the framework of choice for the newgeneration of Web 2.0 developers. Ruby on Rails: Up and Running takes you out for a test drive and shows you just how fastRuby on Rails can go. This compact guide teaches you the basics of installing and using boththe Ruby scripting language and the Rails framework for the quickdevelopment of web applications. Ruby on Rails: Up andRunning covers just about everything youneed - from making a simple database-backed application toadding elaborate Ajaxian features and all the juicy bits in between.While Rails is praised for its simplicity and speed of development,there are still a few steps to master on the way. More advancedmaterial helps you map data to an imperfect table, traverse complexrelationships, and build custom finders. A section on working with Ajaxand REST shows you how to exploit the Rails service frameworks to sendemails, implement web services, and create dynamic user-centric webpages. The book also explains the essentials of logging to findperformance problems and delves into other performance-optimizingtechniques. As new web development frameworks go, Ruby on Rails is the talk of thetown. And Ruby on Rails: Up and Running can makesure you\'re in on the discussion.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Ruby Cookbook: Recipes for Object Oriented Scripting',
             author: 'Lucas Carlson, Leonard Richardson',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596523695.do',
             description: 'Do you want to push Ruby to its limits? The Ruby Cookbook is the most comprehensive problem-solving guide to today\'s hottest programming language. It gives you hundreds of solutions to real-world problems, with clear explanations and thousands of lines of code you can use in your own projects. From data structures and algorithms, to integration with cutting-edge technologies, the Ruby Cookbook has something for every programmer. Beginners and advanced Rubyists alike will learn how to program with: Strings and numbers; Arrays and hashes; Classes, modules, and namespaces; Reflection and metaprogramming; XML and HTML processing; Ruby on Rails (including Ajax integration); Databases; Graphics; Internet services like email, SSH, and BitTorrent; Web services; Multitasking; Graphical and terminal interfaces. If you need to write a web application, this book shows you how to get started with Rails. If you\'re a system administrator who needs to rename thousands of files, you\'ll see how to use Ruby for this and other everyday tasks. You\'ll learn how to read and write Excel spreadsheets, classify text with Bayesian filters, and create PDF files. We\'ve even included a few silly tricks that were too cool to leave out, like how to blink the lights on your keyboard. The Ruby Cookbook is the most useful book yet written about Ruby. When you need to solve a problem, don\'t reinvent the wheel: look it up in the Cookbook.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Making Use of Ruby',
             author: 'Suresh Mahadevan',
             publisher: 'Wiley',
             published: true,
             year: 2003,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780471219729.do',
             description: 'A hands-on introduction to a powerful, easy-to-use scripting language. Ruby\'s clear and clever syntax makes it an excellent choice for beginners, while its speed and extensibility makes it extremely useful for experienced developers. This up-and-coming scripting language is compact, fast, powerful, and yet, surprisingly simple to use. Suresh Mahadevan offers you a detailed introduction to this open source language and explains how it can save programmers considerable time during program development because no compilation and linking is necessary. With its built-in interfaces to Java, Perl, and Python, Ruby is well suited for writing text prototypes, mathematics, and many other everyday programming tasks. Packed with real-world examples and advice on how to move beyond the samples to tackle specific tasks, this excellent resource also includes end-of-chapter reviews and exercises to help you master the many possibilities of Ruby. With numerous real-world examples and expert advice, this book: Provides a concise and task-oriented introduction to Ruby; Helps you evaluate the suitability of Ruby for your project; Is a practical introduction to Ruby\'s syntax and fundamental strengths; Includes numerous working scripts, with information on how to adapt and extend them to meet individual needs.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Ruby Developers Guide',
             author: 'Syngress',
             publisher: 'Elsevier / Syngress',
             published: true,
             year: 2002,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9781928994640.do',
             description: '')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Ruby in a Nutshell',
             author: 'Yukihiro Matsumoto',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2001,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596002145.do',
             description: 'An expert guide to Ruby, a popular new Object-Oriented Programming Language. Ruby is quickly becoming a favourite among developers who need a simple, straight forward, portable programming language. Ruby is ideal for quick and easy object-oriented programming such as processing text files or performing system management. Having been compared with other programming languages such as Perl, Python, PCL, Java, Eiffel, and C++; Ruby is popular because of its straight forward syntax and transparent semantics. Using step-by-step examples and real world applications, the Ruby Developer\'s Guide is designed for programmers and developer\s looking to embrace the object-oriented features and functionality of this robust programming language. Readers will learn how to develop, implement, organize and deploy applications using Ruby. Ruby is currently experiencing a rapid rise in popularity in the object-oriented programming community. Readers receive up-to-the minute links, white papers, and analysis for two years at solutions@syngress.com. Comes with a wallet-sized CD containing a printable HTML version of the book, all of the source code examples and demos of popular Ruby third-party programming tools and applications.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Crafting Rails 4 Applications, 2nd Edition: Expert Practices for Everyday Rails Development',
             author: 'José Valim',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9781937785550.do',
             description: 'Get ready to see Rails as you\'ve never seen it before. Learn how to extend the framework, change its behavior, and replace whole components to bend it to your will. Eight different test-driven tutorials will help you understand Rails\' inner workings and prepare you to tackle complicated projects with solutions that are well-tested, modular, and easy to maintain. This second edition of the bestselling Crafting Rails Applications has been updated to Rails 4 and discusses new topics such as streaming, mountable engines, and thread safety. Rails is one of the most extensible frameworks out there. This pioneering book deep-dives into the Rails plugin APIs and shows you, the intermediate Rails developer, how to use them to write better web applications and make your day-to-day work with Rails more productive.Rails Core developer Jose Valim guides you through eight different tutorials, each using test-driven development to build a new Rails plugin or application that solves common problems with these APIs. You\'ll learn how the Rails rendering stack works and customize it to read templates from the database while you discover how to mimic Active Record behavior, such as validations, in any other object. You\ll find out how Rails integrates with Rack, the different ways to stream data from your web application, and how to mix Rails engines and Sinatra applications into your Rails apps, so you can choose the most appropriate tool for the job. In addition, you\'ll improve your productivity by customizing generators and responders. This book will help you understand Rails\' inner workings, including generators, template handlers, internationalization, routing, and responders. With the knowledge you\'ll gain, you\'ll create well-tested, modular, and robust solutions for your next project.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Working With Unix Processes',
             author: 'Jesse Storimer',
             publisher: 'Self Published',
             published: true,
             year: 2011,
             language: 'en',
             link: 'http://www.jstorimer.com/products/working-with-unix-processes',
             description: 'You\'ll discover... What a file descriptor is and how it works.  When you need a daemon process, and when you don\'t. Creating new processes with fork(2). 4 different ways to exit a process. The real world concerns of spawning shell commands and how to avoid them. High level discussion about the costs and pitfalls of creating processes.Defining signal handlers that don\'t steal from other developers.Internals of Resque and Unicorn and how they use this stuff. Lots more! It\'s 130 pages packed with guidelines, sample code, and best practices.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Working With TCP Sockets',
             author: 'Jesse Storimer',
             publisher: 'Self Published',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://www.jstorimer.com/products/working-with-tcp-sockets',
             description: 'The first section of the book is all about learning the fundamentals of programming with sockets. This includes creating sockets, the client and server lifecycle, and reading/writing data. The second section of the book covers the harder stuff. This includes various methods for optimizing socket operations, the proper way to do socket timeouts in Ruby, SSL sockets, multiplexing connections, and more. The last section applies this knowledge to a real world problem by writing an FTP server. In this section of the book I take a simple, sequential FTP server and rewrite it 6 times using different architecture patterns to demonstrate networking concurrency in Ruby and ways to organize your socket code. You\'ll see these same patterns in libraries like Puma, Unicorn, Thin, and EventMachine. ')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Ruby Way, The',
             author: 'Hal Fulton',
             publisher: 'Sams',
             published: true,
             year: 2001,
             language: 'en',
             link: 'http://www.informit.com/store/ruby-way-9780672320835',
             description: 'The Ruby Way assumes that the reader is already familiar with the subject matter. Using many code samples it focuses on "how-to use Ruby" for specific applications, either as a stand-alone language, or in conjunction with other languages. Topics covered include: Simple data tasks; Manipulating structured data; External data manipulation; User interfaces; Handling threads; System programming; Network and web programming; Tools and utilities. Note: The appendices offer instruction on migrating from Perl and Python to Ruby, and extending Ruby in C and C++.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Eloquent Ruby',
             author: 'Russ Olsen',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2011,
             language: 'en',
             link: 'http://www.informit.com/store/eloquent-ruby-9780321584106',
             description: 'It’s easy to write correct Ruby code, but to gain the fluency needed to write great Ruby code, you must go beyond syntax and absorb the “Ruby way” of thinking and problem solving. In Eloquent Ruby, Russ Olsen helps you write Ruby like true Rubyists do–so you can leverage its immense, surprising power. Olsen draws on years of experience internalizing the Ruby culture and teaching Ruby to other programmers. He guides you to the “Ah Ha!” moments when it suddenly becomes clear why Ruby works the way it does, and how you can take advantage of this language’s elegance and expressiveness. Eloquent Ruby starts small, answering tactical questions focused on a single statement, method, test, or bug. You’ll learn how to write code that actually looks like Ruby (not Java or C#); why Ruby has so many control structures; how to use strings, expressions, and symbols; and what dynamic typing is really good for. Next, the book addresses bigger questions related to building methods and classes. You’ll discover why Ruby classes contain so many tiny methods, when to use operator overloading, and when to avoid it. Olsen explains how to write Ruby code that writes its own code–and why you’ll want to. He concludes with powerful project-level features and techniques ranging from gems to Domain Specific Languages. A part of the renowned Addison-Wesley Professional Ruby Series, Eloquent Ruby will help you “put on your Ruby-colored glasses” and get results that make you a true believer.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Ruby Phrasebook',
             author: 'Jason D. Clinton',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://www.informit.com/store/ruby-phrasebook-9780672328978',
             description: 'Ruby Phrasebook gives you the code you need to quickly and effectively work with Ruby, one of the fastest-growing languages in the world thanks to popular new Ruby technologies like Ruby on Rails. Concise and Accessible. Easy to carry and easy to use—lets you ditch all those bulky books for one portable pocket guide. Flexible and Functional. Packed with more than 100 customizable code snippets—so you can readily code functional Ruby in just about any situation. Jason Clinton uses Ruby daily in system administration and development for Advanced Clustering Technologies, a Linux Beowulf cluster integrator. He has been working in the computer industry for more than a decade and is actively involved in the Kansas City Ruby Users Group (KCRUG), serving as administrator of the group’s web site and mailing list. Register your book at informit.com/register for convenient access to downloads, updates, and corrections as they become available.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Refactoring in Ruby',
             author: 'William C. Wake, Kevin Rutherford',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://www.informit.com/store/refactoring-in-ruby-9780132651400',
             description: 'The First Hands-On, Practical, All-Ruby Refactoring Workbook! Refactoring–the art of improving the design of existing code–has taken the world by storm. So has Ruby. Now, for the first time, there’s a refactoring workbook designed from the ground up for the dynamic Ruby language. Refactoring in Ruby gives you all the realistic, hands-on practice you need to refactor Ruby code quickly and effectively. You’ll discover how to recognize “code smells,” which signal opportunities for improvement, and then perfect your program’s design one small, safe step at a time. The book shows you when and how to refactor with both legacy code and during new test-driven development, and walks you through real-world refactoring in detail. The workbook concludes with several applications designed to help practice refactoring in realistic domains, plus a handy code review checklist you’ll refer to again and again. Along the way, you’ll learn powerful lessons about designing higher quality Ruby software–lessons that will enable you to experience the joy of writing consistently great code.')
             book.tag_list = 'refactoring'
book.save!

 book = Book.new(
             title: 'Design Patterns in Ruby',
             author: 'Russ Olsen',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.informit.com/store/design-patterns-in-ruby-9780321490452',
             description: 'Design Patterns in Ruby documents smart ways to resolve many problems that Ruby developers commonly encounter. Russ Olsen has done a great job of selecting classic patterns and augmenting these with newer patterns that have special relevance for Ruby. He clearly explains each idea, making a wealth of experience available to Ruby developers for their own daily work.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Distributed Programming with Ruby',
             author: 'Mark Bates',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://www.informit.com/store/distributed-programming-with-ruby-9780321638366',
             description: 'Complete, Hands-On Guide to Building Advanced Distributed Applications with Ruby Distributed programming techniques make applications easier to scale, develop, and deploy—especially in emerging cloud computing environments. Now, one of the Ruby community’s leading experts has written the first definitive guide to distributed programming with Ruby. Mark Bates begins with a simple distributed application, and then walks through an increasingly complex series of examples, demonstrating solutions to the most common distributed programming problems. Bates presents the industry’s most useful coverage of Ruby’s standard distributed programming libraries, DRb and Rinda. Next, he introduces powerful third-party tools, frameworks, and libraries designed to simplify Ruby distributed programming, including his own Distribunaut. If you’re an experienced Ruby programmer or architect, this hands-on tutorial and practical reference will help you meet any distributed programming challenge, no matter how complex.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Refactoring: Ruby Edition',
             author: 'Jay Fields, Shane Harvie, Martin Fowler, Kent Beck',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://www.informit.com/store/refactoring-ruby-edition-9780321603500',
             description: 'The Definitive Refactoring Guide, Fully Revamped for Ruby. With refactoring, programmers can transform even the most chaotic software into well-designed systems that are far easier to evolve and maintain. What’s more, they can do it one step at a time, through a series of simple, proven steps. Now, there’s an authoritative and extensively updated version of Martin Fowler’s classic refactoring book that utilizes Ruby examples and idioms throughout–not code adapted from Java or any other environment. The authors introduce a detailed catalog of more than 70 proven Ruby refactorings, with specific guidance on when to apply each of them, step-by-step instructions for using them, and example code illustrating how they work. Many of the authors’ refactorings use powerful Ruby-specific features, and all code samples are available for download. Leveraging Fowler’s original concepts, the authors show how to perform refactoring in a controlled, efficient, incremental manner, so you methodically improve your code’s structure without introducing new bugs. Whatever your role in writing or maintaining Ruby code, this book will be an indispensable resource.')
             book.tag_list = 'refactoring'
book.save!

 book = Book.new(
             title: 'Ruby Way, Second Edition, The: Solutions and Techniques in Ruby Programming, 2nd Edition',
             author: 'Hal Fulton',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://www.informit.com/store/ruby-way-second-edition-solutions-and-techniques-in-9780672328848',
             description: 'Ruby is an agile object-oriented language, borrowing some of the best features from LISP, Smalltalk, Perl, CLU, and other languages. Its popularity has grown tremendously in the five years since the first edition of this book. The Ruby Way takes a “how-to” approach to Ruby programming with the bulk of the material consisting of more than 400 examples arranged by topic. Each example answers the question “How do I do this in Ruby?” Working along with the author, you are presented with the task description and a discussion of the technical constraints. This is followed by a step-by-step presentation of one good solution. Along the way, the author provides detailed commentary and explanations to aid your understanding.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Test Driven Ajax (on Rails,
             description: '')',
             author: 'Phlip',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596510657.do',
             description: 'The World Wide Web has come a long way from static HTML pages. Today\'s developers enforce and enjoy standards, and we have built the web\'s primitive tools into advanced libraries, frameworks, and platforms.With these new freedoms come new responsibilities. Developers can now write some amazing bugs. A bug in a web page, hosted in a free web browser, can render expensive servers useless. Modern editors help rapidly write tangled and crufty code, the perfect habitat for bugs of every species, in situations that are hard to debug. We need help from the mortal enemy of the bug: Test-First Programming. This Short Cut seeks fixes for the hardest situation in web development; proactive test cases for Ajax code. We survey existing techniques, and invent new ones. Our goal is heads-down programming, without repeatedly clicking on a web browser.')
             book.tag_list = 'testing, Ajax'
book.save!

 book = Book.new(
             title: 'Much Ado About Naught',
             author: 'Avdi Grimm',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://devblog.avdi.org/introduction-to-much-ado-about-naught/',
             description: 'An adventure in Ruby metaprogramming! Follow along as we use strict TDD to create a toolkit for generating Null Object classes in Ruby.This is the original source of the Naught gem (http://github.com/avdi/naught)--the first release was written in a Literate Programming style, and this book is the result. Learn numerous metaprogramming tricks, techniques, and gotchas. Watch a library being built in a pure test-driven style. Whimsical illustrations by Lauren Shepard! Kindle, Epub, and HTML versions included (PDF coming soon!). About the beta: this book is basically done, but I\'m still tweaking the output formats.')
             book.tag_list = 'metaprogramming'
book.save!

 book = Book.new(
             title: 'Ajax on Rails: Build Dynamic Web Applications with Ruby',
             author: 'Scott Raymond',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596527440.do',
             description: 'Learn to build dynamic, interactive web applications using the two most important approaches to web development today: Ajax and the phenomenally efficient Ruby on Rails platform. This book teaches intermediate to advanced web developers how to use both Ajax and Rails to quickly build high-performance, scalable applications without being overwhelmed with thousands of lines of JavaScript code. More than just recipes, you also get a thorough, low-level understanding of what\'s happening under the hood. Ajax on Rails includes three fully worked out Rails/Ajax applications, and quick reference sections for Prototype and script.aculo.us.sting lessons show you how to eliminate cross-browser JavaScript errors and DOM debugging nightmares using a combination of Firebug, and Venkman. Advanced material explains the most current design practices for Ajax usability. You\'ll learn to avoid user experience mistakes with proven design patterns. Beyond the how-to, Ajax on Rails helps you consider when Ajax is (and isn\'t) appropriate, and the trade-offs associated with it. For those new to Rails, this book provides a quick introduction, the big picture, a walk through the installation process, and some tips on getting started. If you\'ve already started working with Rails and seek to deepen your skill set, you\'ll find dozens of examples drawn from real-world projects, exhaustive reference for every relevant feature, and expert advice on how to "Ajaxify" your applications.')
             book.tag_list = 'Rails, Ajax'
book.save!

 book = Book.new(
             title: 'RJS Templates for Rails',
             author: 'Cody Fauser',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596528096.do',
             description: 'RJS templates are an exciting and powerful new type of template added to Rails 1.1. Unlike conventional Rails templates that generate HTML or XML, RJS templates generate JavaScript code that is executed when it is returned to the browser. This JavaScript generation allows you to perform multiple page updates in-place without a page reload using Ajax. All the JavaScript you need is generated from simple templates written in Ruby. This document helps you get acquainted with how RJS templates fit into the Rails framework and gets you started with a few easy-to-follow examples.')
             book.tag_list = 'Rails, Ajax'
book.save!

 book = Book.new(
             title: 'Web Services on Rails',
             author: 'Kevin Marshall',
             publisher: 'O\'Reilly Media',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9780596527969.do',
             description: 'This PDF will show you how your small business or enterprise can publish its APIs (application programming interface) to a developer community just like the behemoths of the Internet--Google, Yahoo!, eBay, and Amazon. These giants already offer their APIs to other programmers and allow for add-on services and mash-ups to develop from them--but until recently, the capability for most enterprises to do the same was limited by a myriad of competing standards and a lack of easy-to-use tools to accomplish the task. Ruby on Rails levels the playing field for companies by simplifying the process of building web services and documenting APIs. Now with Rails, enterprise-quality tools are available for all developers. In this document, we\'ll look at how Ruby on Rails makes building web service clients and servers simple and fun. Along the way, we\'ll give working examples and code details so you can see just how everything works.')
             book.tag_list = 'Rails, services'
book.save!

 book = Book.new(
             title: 'From Java To Ruby: Things Every Manager Should Know',
             author: 'Bruce A. Tate',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://pragprog.com/book/fr_j2r/from-java-to-ruby',
             description: 'You want your development team to be productive. You want to write flexible, maintainable web applications. You want to use Ruby and Rails. But can you justify the move away from established platforms such as J2EE? Bruce Tate’s From Java to Ruby has the answers, and it expresses them in a language that’ll help persuade managers and executives who’ve seen it all. See when and where the switch makes sense, and see how to make it.')
             book.tag_list = 'Java'
book.save!

 book = Book.new(
             title: 'Ruby Under a Microscope: An Illustrated Guide to Ruby Internals',
             author: 'Pat Shaughnessy',
             publisher: 'No Starch Press',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://nostarch.com/rum',
             description: 'Ruby is a powerful programming language with a focus on simplicity, but beneath its elegant syntax it performs countless unseen tasks. Ruby Under a Microscope gives you a hands-on look at Ruby’s core, using extensive diagrams and thorough explanations to show you how Ruby is implemented (no C skills required). Author Pat Shaughnessy takes a scientific approach, laying out a series of experiments with Ruby code to take you behind the scenes of how programming languages work. You’ll even find information on JRuby and Rubinius (two alternative implementations of Ruby), as well as in-depth explorations of Ruby’s garbage collection algorithm.')
             book.tag_list = 'ruby-internals'
book.save!

 book = Book.new(
             title: 'Mastering Modern Payments: Using Stripe with Rails',
             author: 'Pete Keen',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'https://www.petekeen.net/mastering-modern-payments',
             description: ' Simple tutorials only go so far. Blog posts from years ago are easy to find, but are they still correct? API documentation doesn\'t help if you don\'t know what you\'re looking for. Mastering Modern Payments: Using Stripe with Rails distills the experience of building dozens of Stripe integrations into one concise, comprehensive guide. From first chapter to last, MMP is packed with advice and examples: Basic Integration guides you through the most basic Stripe billing system possible and prepares your app for the rest of the book. Security and PCI Compliance guides you through the murky waters of the Payment Card Industry Data Security Standards and how they apply to Stripe. It also talks about security scanners and code analysis tools you can use to validate your code isn\t susceptible to things like cross-site-scripting attacks and SQL injections. Custom Payment Forms shows you how to get away from Stripe Checkout and build your own payment forms, your way, with your own framework. State and History teaches you why and how you should build your application with explicit state machines. It also walks through why keeping audit logs of the changes to your your models is so very important, and how youc an get them with just one line of code.Handling Webhooks describes how to handle Stripe\'s webhook events to alert you of errors, perform vital upkeep within your application, and send receipts to customers. Background Workers describes how to communicate with Stripe asynchronously. The Internet is a wild, wacky place, and your application needs to be able to recover from any number of errors, both internal and external. Subscriptions walks through your options for building recurring billing into your application, along with advice about emailing your customers and integrating analytics services. Connect and Marketplaces adds onto the basic integration again, this time allowing your app to sell things on your users\' behalf with Stripe Connect and send them payments with Transfers. MMP has helped a thousand people just like you build their Stripe integrations with confidence, billing customers sooner and more reliabily.')
             book.tag_list = 'Rails, ecommerce'
book.save!

 book = Book.new(
             title: 'Objects on Rails: Notes on flexible web application design',
             author: 'Avdi Grimm',
             publisher: 'Self Published',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://objectsonrails.com/',
             description: 'This is the complete text of Objects on Rails, a "developer\'s notebook" documenting some guidelines, techniques, and ideas for applying classic object-oriented thought to Ruby on Rails applications. This book is aimed at the working Rails developer who is looking to grow and evolve Rails projects while keeping them flexible, maintainable, and robust. The focus is on pragmatic solutions which tread a "middle way" between the expedience of the Rails "golden path", and rigid OO purity. ')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Practical Object-Oriented Design in Ruby: An Agile Primer',
                author: 'Sandi Metz',
                publisher: 'Addison-Wesley Professional',
             year: 2012,
             language: 'en',
                link: 'http://www.informit.com/store/practical-object-oriented-design-in-ruby-an-agile-primer-9780321721334',
             description: 'The Complete Guide to Writing More Maintainable, Manageable, Pleasing, and Powerful Ruby Applications. Ruby’s widely admired ease of use has a downside: Too many Ruby and Rails applications have been created without concern for their long-term maintenance or evolution. The Web is awash in Ruby code that is now virtually impossible to change or extend. This text helps you solve that problem by using powerful real-world object-oriented design techniques, which it thoroughly explains using simple and practical Ruby examples. Sandi Metz has distilled a lifetime of conversations and presentations about object-oriented design into a set of Ruby-focused practices for crafting manageable, extensible, and pleasing code. She shows you how to build new applications that can survive success and repair existing applications that have become impossible to change. Each technique is illustrated with extended examples, all downloadable from the companion Web site, poodr.info. The first title to focus squarely on object-oriented Ruby application design, Practical Object-Oriented Design in Ruby will guide you to superior outcomes, whatever your previous Ruby experience. Novice Ruby programmers will find specific rules to live by; intermediate Ruby programmers will find valuable principles they can flexibly interpret and apply; and advanced Ruby programmers will find a common language they can use to lead development and guide their colleagues.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Writing Efficient Ruby Code (Digital Short Cut,
             description: '')',
             author: 'Stefan Kaes',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.informit.com/store/writing-efficient-ruby-code-digital-short-cut-9780321540034',
             description: 'This short cut focuses on a number of coding patterns that are useful when trying to get maximum speed out of performance-critical sections of Ruby code. Anti-patterns, that is, coding idioms, which should be avoided in performance-sensitive code sections are discussed, including details on how to transform such code to make it more efficient. Most patterns were extracted from Stefan Kaes\' work on improving performance of the Rails core and his regular Rails performance consulting work. These patterns are largely non-algorithmic, detailing local code transformations to achieve identical results with slightly different and faster code, as even local code changes can sometimes result in orders of magnitude improvements. Some patterns are useful independent of Rails\' implementation language, but some of them are specific to Ruby, or more specifically, the current implementation of Ruby. Converts from other languages, especially from statically typed languages such as Java or C++ may find this material useful, as the performance characteristics of certain operations, like performing a function call or accessing object fields/attributes, are quite different from what you expect.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'RailsSpace: Building a Social Networking Website with Ruby on Rails™',
             author: 'Michael Hartl, Aurelius Prochazka',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.informit.com/store/railsspace-building-a-social-networking-website-with-9780321480798',
             description: 'Ruby on Rails is fast displacing PHP, ASP, and J2EE as the development framework of choice for discriminating programmers, thanks to its elegant design and emphasis on practical results. RailsSpace teaches you to build large-scale projects with Rails by developing a real-world application: a social networking website like MySpace, Facebook, or Friendster. Inside, the authors walk you step by step from the creation of the site\'s virtually static front page, through user registration and authentication, and into a highly dynamic site, complete with user profiles, image upload, email, blogs, full-text and geographical search, and a friendship request system. In the process, you learn how Rails helps you control code complexity with the model-view-controller (MVC) architecture, abstraction layers, automated testing, and code refactoring, allowing you to scale up to a large project even with a small number of developers. This essential introduction to Rails provides: A tutorial approach that allows you to experience Rails as it is actually used; A solid foundation for creating any login-based website in Rails; Coverage of newer and more advanced Rails features, such as form generators, REST, and Ajax (including RJS); A thorough and integrated introduction to automated testing. The book\'s companion website provides the application source code, a blog with follow-up articles, narrated screencasts, and a working version of the RailSpace social network.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Sams Teach Yourself Ruby in 21 Days',
             author: 'Mark Slagell',
             publisher: 'Sams',
             published: true,
             year: 2002,
             language: 'en',
             link: 'http://www.informit.com/store/sams-teach-yourself-ruby-in-21-days-9780672322525',
             description: 'Ruby is a high-level, fully object-oriented programming (OOP) language. It is very powerful and relatively easy to learn, read, and maintain. Sams Teach Yourself Ruby in 21 Days provides the best introduction to this language and addresses one of the key constraints it faces: "The paucity of quality English-language documentation is one of the few things holding Ruby back from widespread adoption," according to Dr. Curtis Clifton of Iowa State University¿s Department of Graduate Computer Science.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Troubleshooting Ruby Processes: Leveraging System Tools when the Usual Ruby Tricks Stop Working (Digital Short Cut,
             description: '')',
             author: 'Philippe Hanrigou',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.informit.com/store/troubleshooting-ruby-processes-leveraging-system-tools-9780321544681',
             description: 'This short cut introduces key system diagnostic tools to Ruby developers creating and deploying web applications. When programmers develop a Ruby application they commonly experience complex problems which require some understanding of the underlying operating system to be solved. Difficult to diagnose, these problems can make the difference between a project\'s failure or success. This short cut demonstrates how to leverage system tools available on Mac OS X, Linux, Solaris, BSD or any other Unix flavor. You will learn how to leverage the raw power of tools such as lsof, strace or gdb to resolve problems that are difficult to diagnose with the standard Ruby development tools. You will also find concrete examples that illustrate how these tools solve real-life problems in Ruby development. This expertise will prove especially relevant during the deployment phase of your application. In this way, should your production Mongrel cluster freeze and stop serving HTTP requests, it will not take you 2 days to figure out why!')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Mongrel (Digital Shortcut): Serving, Deploying, and Extending Your Ruby Applications',
             author: 'Matt Pelletier, Zed Shaw',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://www.informit.com/store/mongrel-digital-shortcut-serving-deploying-and-extending-9780321483508',
             description: 'The expert guide to building Ruby on Rails applications. Ruby on Rails strips complexity from the development process, enabling professional developers to focus on what matters most: delivering business value. Now, for the first time, there’s a comprehensive, authoritative guide to building production-quality software with Rails. Pioneering Rails developer Obie Fernandez and a team of experts illuminate the entire Rails API, along with the Ruby idioms, design approaches, libraries, and plug-ins that make Rails so valuable. Drawing on their unsurpassed experience, they address the real challenges development teams face, showing how to use Rails’ tools and best practices to maximize productivity and build polished applications users will enjoy. Using detailed code examples, Obie systematically covers Rails’ key capabilities and subsystems. He presents advanced programming techniques, introduces open source libraries that facilitate easy Rails adoption, and offers important insights into testing and production deployment. Dive deep into the Rails codebase together, discovering why Rails behaves as it does– and how to make it behave the way you want it to. ')
             book.tag_list = 'deployment'
book.save!

 book = Book.new(
             title: 'Rails Way, The',
             author: 'Obie Fernandez',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.informit.com/store/rails-way-9780321445612',
             description: '')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Rails 3 Way, The, 2nd Edition',
             author: 'Obie Fernandez',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2010,
             language: 'en',
             link: 'http://www.informit.com/store/rails-3-way-9780321601667',
             description: 'The Bible for Ruby on Rails Application Development. Ruby on Rails strips complexity from the development process, enabling professional developers to focus on what matters most: delivering business value via clean and maintainable code. The Rails™ 3 Way is the only comprehensive, authoritative guide to delivering production-quality code with Rails 3. Pioneering Rails expert Obie Fernandez and a team of leading experts illuminate the entire Rails 3 API, along with the idioms, design approaches, and libraries that make developing applications with Rails so powerful. Drawing on their unsurpassed experience and track record, they address  the real challenges development teams face, showing how to use Rails 3 to maximize your productivity. Using numerous detailed code examples, the author systematically covers Rails 3 key capabilities and subsystems, making this book a reference that you will turn to again and again. He presents advanced Rails programming techniques that have been proven effective in day-to-day usage on dozens of production Rails systems and offers important insights into behavior-driven development and production considerations such as scalability. Dive deep into the Rails 3 codebase and discover why Rails is designed the way it is—and how to make it do what you want it to do.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Rubyisms in Rails (Digital Short Cut)',
             author: 'Jacob Harris',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://www.informit.com/store/rubyisms-in-rails-digital-short-cut-9780321474070',
             description: 'Rubyisms is an examination of how the style of Ruby informs the design of Rails. In particular, it looks at a few specific examples of how Rails\' internal code is implemented in Ruby to instruct about Ruby\'s design principles. The main goal is simply aesthetic appreciation. But, if you are a beginning programmer in Rails who is stymied in your understanding of Ruby–or an intermediate Rails developer still writing code that looks like Ruby-tinged PHP or Java–this Short Cut will hopefully impart enlightenment and inspiration about the Ruby way of programming. It also reveals how the revolutionary design of the Rails framework can only be built upon the beauty of Ruby.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'IronRuby Unleashed',
             author: 'Shay Friedman',
             publisher: 'Sams',
             published: true,
             year: 2010,
             language: 'en',
             link: 'http://www.informit.com/store/ironruby-unleashed-9780672330780',
             description: 'Ruby has built an enormous following of developers attracted by its intuitiveness, flexibility, and simplicity. Meanwhile, Microsoft’s .NET has grown and matured into a platform of unparalleled power. IronRuby brings them together, enabling developers to write elegant, efficient Ruby code that seamlessly integrates with .NET objects and leverages .NET’s full capabilities. Now, in IronRuby Unleashed, one of IronRuby’s most respected early adopters demonstrates how to write outstanding production software with the brand new IronRuby 1.0. Writing for both Ruby and .NET developers, Shay Friedman covers every facet of IronRuby programming. Friedman begins by explaining how IronRuby leverages the new Dynamic Language Runtime (DLR) libraries to run atop the .NET Framework and access its resources. Next, he presents an in-depth IronRuby tutorial that ranges from basic syntax and object-oriented programming techniques through advanced concepts. Building on this foundation, you’ll learn how to make the most of a broad spectrum of .NET platform features. IronRuby Unleashed thoroughly illuminates the use of IronRuby and .NET with today’s most powerful frameworks and technologies, including WPF, ASP.NET MVC, Ruby on Rails, and Silverlight. You’ll also find detailed coverage of unit testing, as well as cutting-edge techniques for extending IronRuby with C# or VB.NET. Detailed information on how to... Install IronRuby and choose the right development environment for your needs; Master IronRuby syntax, methods, blocks, classes, modules, libraries, and more; Write code that takes advantage of IronRuby’s dynamic and metaprogramming features; Utilize .NET services and frameworks to write more powerful Ruby code than ever before; Incorporate efficient data access into your IronRuby applications; Use IronRuby to build Windows software with both WinForms and WPF; Rapidly build high-quality Web applications with IronRuby and Ruby on Rails; Create rich Web 2.0 applications with IronRuby and Microsoft Silverlight; Test .NET code with Ruby’s leading unit testing frameworks; Run IronRuby code from other .NET code, and create .NET code libraries that fit well with IronRuby code.')
             book.tag_list = 'IronRuby'
book.save!

 book = Book.new(
             title: 'Rails Routing (Digital Shortcut,
             description: '')',
             author: 'David A. Black',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.informit.com/store/rails-routing-digital-shortcut-9780321509246',
             description: 'The Rails routing system has always been a key component of theframework, and with the introduction of RESTful routes in the pastyear, it has taken center stage. Fully programmable, the routing systemgoverns the process of mapping request URLs to the appropriatecontroller action. Routing also helps you when you’re writing code,especially view templates, where the same logic that interprets URLsworks in reverse to automate the generation of URL strings in yourcode. As all routing is rule-based, you have to know how to write therules to get the most out of the routing system. In this short cut, you’lllearn techniques for writing custom routing rules, how to tap into theconvenience and power of named routes, and the workings of theRESTful routing that’s had such an impact on Rails development. Along with a thorough introduction to routing syntax and semantics,you’ll find techniques for testing and troubleshooting routes, and tipson the use of this important part of your Rails skill set.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Flex on Rails: Building Rich Internet Applications with Adobe Flex 3 and Rails 2',
             author: 'Tony Hillerson, Daniel Wanja',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://www.informit.com/store/flex-on-rails-building-rich-internet-applications-with-9780132651554',
             description: 'Adobe Flex enables the rapid development of rich and engaging user experiences. Ruby on Rails dramatically simplifies the development of database-driven web applications. Now there’s a book that shows how to use the newest versions of both frameworks together to create state-of-the-art Rich Internet Applications (RIAs). Flex on Rails begins with the absolute essentials: setting up your environment for Flex and Rails, passing data with XML, and integrating Flex front-ends with Rails back-ends. Then, using practical, easy-to-understand code examples, the authors take you from the basics to advanced topics only discussed in this book. Techniques covered here include: Constructing sophisticated interfaces that can’t be created with AJAX alone; Using RESTful services to expose applications for access via APIs; Testing Flex and Rails together; Using Flex Frameworks; Getting Flex into your build/deploy process; And more… The authors also offer practical introductions to powerful complementary technologies, such as RubyAMF and Juggernaut. Written by developers with extensive experience using both frameworks, this book covers the new Adobe Flex 3 and Ruby on Rails 2 from the ground up. Even if you have minimal experience with Flex or Rails, you’ll learn all you need to know to use them to build exceptional production applications.')
             book.tag_list = 'Flex'
book.save!

 book = Book.new(
             title: 'Merb Way, The',
             author: 'Foy Savas',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://www.informit.com/store/merb-way-9780132651684',
             description: 'The Expert Guide to Building Merb Applications.Thousands of Ruby and Rails developers are discovering the extraordinary scalability, agility, flexibility, and performance offered by the new Merb MVC framework. The Merb Way is the first comprehensive guide to using, extending, and deploying Merb. Like the bestseller The Rails Way (Addison-Wesley, 2008), this book can either be read cover-to-cover as a tutorial or used for modular coverage that makes it an ideal task reference. Foy Savas systematically covers everything developers need to know to build production-quality Merb applications, offering detailed code examples for jumpstarting virtually any project. Savas is not only involved in the Merb project as an open source contributor: He uses Merb every day as a professional developer. Drawing on his extensive practical expertise, he delves deeply into the Merb framework’s architecture and source code, revealing its elegance and offering powerful best practices for using it. To maximize this book’s practical value, he also covers the tools most widely used alongside Merb, including the DataMapper ORM, the RSpec tester (and associated behavior-driven development techniques), and several leading Merb plugins.')
             book.tag_list = 'Merb'
book.save!

 book = Book.new(
             title: 'Security on Rails',
             author: 'Ben Poweski, David Raphael',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://shop.oreilly.com/product/9781934356487.do',
             description: 'The advantage of using Rails is its agility; it makes developing your web applications easy and fast. The disadvantage is that it can leave holes in your security if you are not aware of common vulnerabilities. It\'s a nerve-wracking and unfortunate fact that there are plenty of malicious people lurking on the Web. As a Rails developer, it is essential that you understand how to assess risk and protect your data and your users. Security on Rails uses established security principles to teach you how to write more secure software, defend your applications from common threats, and encrypt your data. We\'ll give you an example of a hacking exploit, and explore how to fix the weaknesses in an application. You\'ll learn the steps you need to take to control access to information and authenticate users, including cryptography concepts and authorization. In addition, you\'ll see how to integrate your applications with external management systems; in short, the crucial details you must consider to protect yourself and your data. The most important element of security is to plan for it before it becomes an issue. Security on Rails helps beginner and intermediate developers to take control of their applications and guard against attacks.')
             book.tag_list = 'Rails, security'
book.save!

 book = Book.new(
             title: 'Using JRuby: Bringing Ruby to Java',
             author: 'Charles O Nutter, Thomas Enebo, Nick Sieger, Ola Bini, Ian Dees',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2011,
             language: 'en',
             link: 'http://pragprog.com/book/jruby/using-jruby',
             description: 'Now you can bring the best of Ruby into the world of Java, with Using JRuby. Come to the source for the JRuby core team’s insights and insider tips. You’ll learn how to call Java objects seamlessly from Ruby, and deal with Java idioms such as interfaces and overloaded functions. Run Ruby code from Java, and make a Java program scriptable in Ruby. See how to compile Ruby into .class files that are callable from Java, Scala, Clojure, or any other JVM language.')
             book.tag_list = 'JRuby'
book.save!

 book = Book.new(
             title: 'Service-Oriented Design with Ruby and Rails',
             author: 'Paul Dix',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2010,
             language: 'en',
             link: 'http://www.informit.com/store/service-oriented-design-with-ruby-and-rails-9780321659361',
             description: ' The Complete Guide to Building Highly Scalable, Services-Based Rails Applications. Ruby on Rails deployments are growing, and Rails is increasingly being adopted in larger environments. Today, Rails developers and architects need better ways to interface with legacy systems, move into the cloud, and scale to handle higher volumes and greater complexity. In Service-Oriented Design with Ruby and Rails Paul Dix introduces a powerful, services-based design approach geared toward overcoming all these challenges. Using Dix’s techniques, readers can leverage the full benefits of both Ruby and Rails, while overcoming the difficulties of working with larger codebases and teams. Dix demonstrates how to integrate multiple components within an enterprise application stack; create services that can easily grow and connect; and design systems that are easier to maintain and upgrade. Key concepts are explained with detailed Ruby code built using open source libraries such as ActiveRecord, Sinatra, Nokogiri, and Typhoeus. The book concludes with coverage of security, scaling, messaging, and interfacing with third-party services.')
             book.tag_list = 'Rails, services'
book.save!

 book = Book.new(
             title: 'Rails AntiPatterns: Best Practice Ruby on Rails Refactoring',
             author: 'Chad Pytel, Tammer Saleh',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2010,
             language: 'en',
             link: 'http://www.informit.com/store/rails-antipatterns-best-practice-ruby-on-rails-refactoring-9780321604811',
             description: 'The book is organized into concise, modular chapters—each outlines a single common AntiPattern and offers detailed, cookbook-style code solutions that were previously difficult or impossible to find. Leading Rails developers Chad Pytel and Tammer Saleh also offer specific guidance for refactoring existing bad code or design to reflect sound object-oriented principles and established Rails best practices. With their help, developers, architects, and testers can dramatically improve new and existing applications, avoid future problems, and establish superior Rails coding standards throughout their organizations.')
             book.tag_list = 'Rails, refactoring'
book.save!

 book = Book.new(
             title: 'Beginning Rails',
             author: 'Cloves Carneiro Jr. , Hampton Catlin , Jeffrey Allan Hardy',
             publisher: 'apress',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.apress.com/9781590596869',
             description: 'Beginning Rails is the practical starting point for anyone wanting to learn how to build dynamic web applications using the Rails framework for Ruby. You’ll learn how all of the components of Rails fit together and how you can leverage them to create sophisticated web applications with less code and more joy. This book is particularly well suited to those with little or no experience with web application development, or who have some experience but are new to Rails. Beginning Rails assumes basic familiarity with web terms and technologies, but doesn\'t require you to be an expert. Rather than delving into the arcane details of Rails, the focus is on the aspects of the framework that will become your pick, shovel, and axe. Part history lesson, part introduction to object-oriented programming, and part dissertation on open source software, Beginning Rails doesn’t just explain how to do something in Rails, it explains why. Every programmer fondly remembers the book that helped them get started. The goal of Beginning Rails is to become that book for you, today.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Foundation Rails 2',
             author: 'Eldon Alameda',
             publisher: 'apress',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://www.apress.com/9781430210399',
             description: 'You\'ve heard about this amazing web framework called Ruby on Rails that\'s taken the world by storm, but thought it was way too complex for you? Think again. Foundation Rails 2 takes you through your first steps in Rails, explaining in plain English how to start building dynamic web applications. And there\'s never been a better time to jump in to the Rails world, as the release of Rails 2 was a major evolutionary leap forward from previous versions. Unlike other Rails books, this book doesn\'t throw you into the deep end right away in the hopes that you\'ll learn to swim. Instead, we\'ll start out with the basics and continually expand your knowledge until, by the end of the book, we\'re building a Rails application with dynamic features such as user registration, geocoding, filtering results with AJAX, RSS feeds, and an XML interface. Meanwhile, we\'ll talk about the important issues that other books often leave out such as testing your application\'s code, securing your application from hackers, optimizing your code for the best performance, and of course, deploying your application. This book takes a focused approach to guiding you through understanding how the pieces of Rails work individually and how they fit together. Instead of emphasizing boring theoretical discussions, Foundation Rails 2 lets you get your hands dirty with the framework and learn the hows and whys of Rails faster than ever. We start with a tour of what makes Rails special and why you need to learn it, move into a gentle introduction to the high points of programming in Ruby, and then take a tour of a sample Rails application. Next, we dig deeper into the core components of Rails before building a complete Rails application together. By the end of this book, not only will you know how to build Rails applications but you\'ll understand the reasons behind what you do.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Practical Rails Social Networking Sites',
             author: 'Alan Bradburne',
             publisher: 'apress',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.apress.com/9781590598412',
             description: 'Practical Rails Social Networking Sites shows you the complete development cycle of a social networking community web site. The project develops first as a simple content management system, after which author Alan Bradburne progressively adds features in order to build a full Web 2.0-enabled community-based social networking site using Ruby on Rails. You will learn how to make the best use of the Ruby on Rails framework within a large project and how to implement and adapt features specific to a community. The book offers practical advice and tips for developing and testing, along with guidance on how to take your site live, as well as optimize and maintain it. The book also explores how to integrate with other community sites such as Flickr and Google Maps, and how to make good use of Rails Ajax features. You will also learn how to optimize and adapt your site to work well on mobile browsers. ')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Practical Rails Projects',
             author: 'Eldon Alameda',
             publisher: 'apress',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.apress.com/9781590597811',
             description: 'The best way to learn Rails is by creating a variety of applications with it. You already know the basics of Rails, and you’re familiar with the exciting features and benefits associated with using this Ruby–based framework. You’re now at the point where you need to gain firsthand experience with Rails by thoroughly exploring the features and building several different types of web applications. Eldon Alameda takes a focused approach to guiding you through the creation of multiple real-world examples that are designed to get your hands dirty with the core features of Rails, while providing you with the valuable experience of creating real Rails applications. Projects you’ll work on include creating a simple blog with an external API, constructing a workout tracker with a RESTful interface and graphs, and converting an existing PHP site to Rails while adding an advanced JavaScript interface. As an added bonus, the final project makes use of the edge version of Rails as you build an application that utilizes Active Resource, which provides an opportunity to explore the various changes and features that will be included with Rails 2.0. Each project is designed to provide you with the necessary information and tools to give you a running start at solving that problem yourself, and each project includes a number of additional ideas and exercises for ways that you can extend each application to fit your own needs.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Practical JRuby on Rails Web 2.0 Projects',
             author: 'Ola Bini',
             publisher: 'apress',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.apress.com/9781590598818',
             description: 'Discover how JRuby on Rails can be used to create web applications faster and more efficiently while still taking advantage of the vast power of the Java platform. Ruby on Rails is proving itself to be one of the most efficient and powerful agile web development application frameworks available and has had a profound influence on the Java community. The JRuby project offers Java developers the best of two worlds: the flexibility of Ruby on Rails coupled with the enterprise-level power and maturity of the Java platform. JRuby core developer Ola Bini covers everything you need to know to take full advantage of what JRuby has to offer, including: Full coverage on how to use JRuby to create web applications faster and more efficiently, while continuing to take advantage of the vast power of the Java platform. Several real-world projects that illustrate the crucial specifics you need to know about the interaction of Java and Ruby. Helpful, practical instruction and discussion on how web applications can be deployed using a variety of popular servers such as Apache and Mongrel.')
             book.tag_list = 'Rails, JRuby'
book.save!

 book = Book.new(
             title: 'Deploying with JRuby: Deliver Scalable Web Apps using the JVM',
             author: 'Joe Kutner',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://pragprog.com/book/jkdepj/deploying-with-jruby',
             description: 'Deploy using the JVM’s high performance while building your apps in the language you love. JRuby is a fast, scalable, and powerful JVM language with all the benefits of a traditional Ruby environment. See how to consolidate the many moving parts of an MRI-based Ruby deployment onto a single JVM process. You’ll learn how to port a Rails application to JRuby, get it into production, and keep it running.')
             book.tag_list = 'JRuby, deployment'
book.save!

 book = Book.new(
             title: 'Everyday Scripting with Ruby: for Teams, Testers, and You',
             author: 'Brian Marick',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://pragprog.com/book/bmsft/everyday-scripting-with-ruby',
             description: ' Are you a tester who spends more time manually creating complex test data than using it? A business analyst who seemingly went to college all those years so you can spend your days copying data from reports into spreadsheets? A programmer who can’t finish each day’s task without having to scan through version control system output, looking for the file you want? If so, you’re wasting that computer on your desk. Offload the drudgery to where it belongs, and free yourself to do what you should be doing: thinking. All you need is a scripting language (free!), this book (cheap!), and the dedication to work through the examples and exercises.')
             book.tag_list = 'scripting'
book.save!

 book = Book.new(
             title: 'Agile Web Development with Rails (3rd edition,
             description: '')',
             author: 'Sam Ruby, Dave Thomas, David Heinemeier Hansson',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://pragprog.com/book/rails3/agile-web-development-with-rails',
             description: 'Rails just keeps on changing. Both Rails 3 and 4, as well as Ruby 1.9 and 2.0, bring hundreds of improvements, including new APIs and substantial performance enhancements. The fourth edition of this award-winning classic has been reorganized and refocused so it’s more useful than ever before for developers new to Ruby and Rails. Rails 4 introduces a number of user-facing changes, and the book has been updated to match all the latest changes and new best practices in Rails. This includes full support for Ruby 2.0, controller concerns, Russian Doll caching, strong parameters, Turbolinks, new test and bin directory layouts, and much more.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Practical Rails Plugins',
             author: 'Nick Plante , David Berube',
             publisher: 'apress',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://www.apress.com/9781590599938',
             description: 'The Rails framework empowers developers with unprecedented levels of productivity by embracing principles such as “convention over configuration” and “don’t repeat yourself”. This paradigm is even apparent at the community level, where developers regularly share their custom framework extensions by way of Rails’ plugins feature. Plugins offer a way for developers to extend the core Rails framework with their own custom features, allowing for rapid integration of features such as authentication, user ratings, and search. Practical Rails Plugins shows you how to capitalize upon the wide variety of plugins at your disposal by guiding you through their integration into a number of interesting projects. You’ll learn how to rapidly augment projects involving asynchronous video transcoding, geocoding and mapping, content management, community ratings, and PDF generation. You’ll also learn how to create and distribute your own plugins. The ultimate guide to building powerful web sites faster using Rails plugins. Demonstrates how to use popular plugins within a number of practical (and fully functional) projects and mini–applications. Shows you how to create and distribute your own plugins.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Ruby on Rails Tutorial 2nd edition: Learn Web Development with Rails (with Rails 4 chapter)',
             author: 'Michael Hartl',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://ruby.railstutorial.org/buy',
             description: '')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Beginning Google Maps Applications with Rails and Ajax',
             author: 'Rails ajax',
             publisher: 'apress',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.apress.com/9781590597873',
             description: 'The Google Maps API remains one of the showcase examples of the Web 2.0 development paradigm. Beginning Google Maps Applications with Rails and Ajax: From Novice to Professional is the first book to comprehensively introduce the service from a developer perspective, showing you how you can integrate mapping features into your Rails-driven web applications. Proceeding far beyond simplistic map display, youll learn how to draw from a variety of data sources such as the U.S. Census Bureau\'s TIGER/Line data and Google\'s own geocoding feature to build comprehensive geocoding services for mapping many locations around the world. The book also steers you through various examples that show how to encourage user interaction such as through pinpointing map locations, adding comments, and building community-driven maps. Youll want to pick up a copy of this book because. This is the first book to comprehensively introduce the Google Maps application development using the Rails development framework. Youll be introduced to the very latest changes to the Google Maps API, embodied in the version 2 release. It is written by four developers actively involved in the creation of location-based mapping services. For additional info, please visit the author\'s reference site for this book. ')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Deploying Rails: Automate, Deploy, Scale, Maintain, and Sleep at Night',
             author: 'Tom Copeland, Anthony Burns',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://pragprog.com/book/cbdepra/deploying-rails',
             description: 'Today’s modern Rails applications have lots of moving parts. Make sure your next production deployment goes smoothly with this hands-on book, which guides you through the entire production process. You’ll set up scripts to install and configure all the software your servers need, including your application code. Once you’re in production, you’ll learn how to set up systems to monitor your application’s health, gather metrics so you can stop problems before they start, and fix things when they go wrong.')
             book.tag_list = 'deployment'
book.save!

 book = Book.new(
             title: 'The Cucumber Book: Behaviour-Driven Development for Testers and Developers',
             author: 'Matt Wynne, Aslak Hellesøy',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://pragprog.com/book/hwcuc/the-cucumber-book',
             description: 'Your customers want rock-solid, bug-free software that does exactly what they expect it to do. Yet they can’t always articulate their ideas clearly enough for you to turn them into code. The Cucumber Book dives straight into the core of the problem: communication between people. Cucumber saves the day; it’s a testing, communication, and requirements tool – all rolled into one.')
             book.tag_list = 'testing'
book.save!

 book = Book.new(
             title: 'Programming Ruby 1.9 (3rd edition): The Pragmatic Programmers\' Guide',
             author: 'Dave Thomas, with Chad Fowler, Andy Hunt',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://pragprog.com/book/ruby3/programming-ruby-1-9',
             description: 'Ruby is the fastest growing and most exciting dynamic language out there. If you need to get working programs delivered fast, you should add Ruby to your toolbox.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Programming Ruby (2nd edition,
             description: ''): The Pragmatic Programmers\' Guide',
             author: 'Dave Thomas, with Chad Fowler, Andy Hunt',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2004,
             language: 'en',
             link: 'http://pragprog.com/book/ruby/programming-ruby',
             description: 'The Pickaxe book, named for the tool on the cover, is the definitive reference to Ruby, a highly-regarded, fully object-oriented programming language. This Second Edition has more than 200 pages of new content, and substantial enhancements to the original, covering all the new and improved language features of Ruby 1.8 and standard library modules.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Rails for .NET Developers',
             author: 'Jeff Cohen, Brian Eng',
             publisher: 'Pragmatic Bookshelf',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://pragprog.com/book/cerailn/rails-for-net-developers',
             description: 'Rails for .NET Developers introduces the joy of Ruby on Rails development to Microsoft .NET developers. This book demonstrates the essential elements of both the Ruby language and the Rails application framework, geared especially for developers already fluent in the Microsoft .NET platform.')
             book.tag_list = 'Windows'
book.save!

 book = Book.new(
             title: 'The Bastards Book of Ruby: A Programming Primer for Counting and Other Unconventional Tasks',
             author: 'Dan Nguyen',
             publisher: 'Self Published',
             published: true,
             year: 2011,
             language: 'en',
             link: 'http://ruby.bastardsbook.com/',
             description: ' The Bastards Book of Ruby is an introduction to programming and its practical uses for journalists, researchers, scientists, analysts, and anyone else whose job is to seek out, make sense from, and show the hard-to-find data. This does not require being "good at computers", having a background in programming, or the desire (yet) to be a full-fledged hacker/developer. It just takes an eagerness to be challenged.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Ruby on Rails Tutorial: Learn Web Development with Rails',
             author: 'Michael Hartl',
             publisher: 'Self Published',
             published: true,
             year: 2010,
             language: 'en',
             link: 'http://ruby.railstutorial.org/',
             description: 'The Ruby on Rails Tutorial book and screencast series teach you how to develop and deploy real, industrial-strength web applications with Ruby on Rails, the open-source web framework that powers top websites such as Twitter, Hulu, GitHub, and the Yellow Pages. The Ruby on Rails Tutorial book is available for free online and is available for purchase as an ebook (PDF, EPUB, and MOBI formats). The companion screencast series includes 12 individual lessons, one for each chapter of the Ruby on Rails Tutorial book. All purchases also include a free copy of the Solutions Manual for Exercises, with solutions to every exercise in the book.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Clean Ruby: Write obvious, intention-revealing programs that stay manageable as your code grows',
             author: 'Jim Gay',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://www.clean-ruby.com/',
             description: 'Writing small and easily testable methods can certainly clean things up, but as your application grows your classes start to know too much. Large classes means a large cognitive overhead for understanding how things work, or why they don’t. It would be great to keep related things closer together. If this needs that to function, why aren\'t they right in front of your face? You might have broken things up well, but each time you search around for related code it begins to feel like a mess.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Why\'s Poignant Guide to Ruby',
             author: 'Why the Lucky Stiff',
             publisher: 'Self Published',
             published: true,
             year: 2005,
             language: 'en',
             link: 'http://mislav.uniqpath.com/poignant-guide/',
             description: '')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Mr. Neighborly\'s Humble Little Ruby Book',
             author: 'Jeremy McAnally',
             publisher: 'Self Published',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://www.humblelittlerubybook.com/',
             description: ' An up to date book on Ruby programming, written in a style described as "a beautiful display of pragmatically chunky bacon, wrapped in a nutshell." Or something like that. Mr. Neighborly\'s Humble Little Ruby Book covers the Ruby language from the very basics of using puts to put naughty phrases on the screen all the way to serving up your favorite web page from WEBrick or connecting to your favorite web service. Written in a conversational narrative rather than like a dry reference book, Mr. Neighborly\'s Humble Little Ruby Book is an easy to read, easy to follow guide to all things Ruby.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Kestrels, Quirky Birds, and Hopeless Egocentricity',
             author: 'Reginald Braithwaite',
             publisher: 'Self Published',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://combinators.info/',
             description: '')
             book.tag_list = 'metaprogramming'
book.save!

 book = Book.new(
             title: 'Ruby Hacking Guide',
             author: 'Minero AOKI',
             publisher: 'Self Published',
             published: true,
             year: 2004,
             language: 'en',
             link: 'http://ruby-hacking-guide.github.io/',
             description: 'This is a new effort to gather efforts to help translate Ruby Hacking Guide into English. The official support site of the original book is http://i.loveruby.net/ja/rhg/ You can download the version of the source code explained and the tool used in the book from the official support site of the original book.')
             book.tag_list = 'ruby-internals'
book.save!

 book = Book.new(
             title: 'Everyday Rails Testing with RSpec: A practical approach to test-driven development',
             author: 'Aaron Sumner',
             publisher: 'Self Published',
             published: true,
             year: 2012,
             language: 'en',
             link: 'https://leanpub.com/everydayrailsrspec?',
             description: 'Have you gotten your hands dirty with a Rails application or two, but lack reliable test coverage? Does your application testing consist of a series of browser clicks, hoping you cover everything? Or do you just cross your fingers and hope for the best that everything will just work? Don\'t worry, everyone has been there at some point--and while testing and test-driven development are important aspects of Rails development, many tutorials gloss over these components. In Everyday Rails Testing with RSpec, I\'ll show you how I got past that hurdle, increasing my code\'s trustworthiness and saving untold time in browser-based testing.Everyday Rails Testing with RSpec contains six chapters based on content from the Everyday Rails blog, along with six additional chapters exclusive to the book and complete code for a simple, but tested Rails application.')
             book.tag_list = 'Rails, testing'
book.save!

 book = Book.new(
             title: 'Rails As She Is Spoke',
             author: 'Giles Bowkett',
             publisher: 'Self Published',
             published: true,
             year: 2012,
             language: 'en',
             link: 'http://railsoopbook.com/',
             description: 'Do you want to understand Rails? Do you want to write Rails apps effectively, or see your own open source creations delight their users and enjoy wild success like Rails has? Have you noticed that Rails never quite makes perfect sense according to traditional object-oriented theory, or that the Ruby On Rails Guides never seem to quite explain the realities of cutting-edge Rails development?')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Software As A Disservice',
             author: 'Giles Bowkett',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://gilesbowkett.blogspot.com/2013/10/new-ebook-software-as-disservice-fixing.html',
             description: '')
             book.tag_list = 'Rails, refactoring'
book.save!

 book = Book.new(
             title: 'Unfuck A Monorail For Great Justice',
             author: 'Giles Bowkett',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://gilesbowkett.blogspot.com/2013/03/new-ebook-unfuck-monorail-for-great.html',
             description: 'Have you ever encountered a monolithic Rails app? Have you ever wondered how to get it back to the slim, flexible dexterity that it had in the early days of its development? Would you like to know how to make large Rails test suites fast? I\'ve written a book which tells you how to do those things. But that\s not all it does. I\'ve figured out some amazing hacks to accelerate the process of refactoring. Using these techniques, I shrank a code base by over 1100 lines in a single week. Monolithic Rails apps -- or monorails -- are a problem in the world of Rails development. My new book doesn\'t just show you how to get them back on track. It shows you how to get them back on track more cleanly and more swiftly than you would have believed humanly possible.')
             book.tag_list = 'Rails, refactoring'
book.save!

 book = Book.new(
             title: 'Upgrading to Rails 4',
             author: 'Andy Lindeman',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://www.upgradingtorails4.com/',
             description: '')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Upgrade to Rails 4: A thorough guide for upgrading your Rails 3 app',
             author: 'Philip De Smedt',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'https://leanpub.com/upgradetorails4',
             description: 'Covering new Rails 4 features such as Turbolinks, Live Streaming and Strong Parameters! Have you been thinking of upgrading your Rails application to Rails 4 but don\'t know where to start? Are you curious about the new features Rails 4 introduces? Then Upgrade to Rails 4 is the perfect book for you. Rails 4 introduces an abundance of new features. Learn about these to stay up to date with the latest developments, and modernize your application. All deprecated features will be covered in depth along with an explanation of how to replace them. Using the included checklist, you can verify that you have updated all breaking changes when upgrading your Rails 3 application. Upgrade to Rails 4 contains more than 32 chapters covering important new and deprecated features in Rails 4.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Learn Ruby on Rails',
             author: 'Daniel Kehoe',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://learn-rails.com/',
             description: 'Get a solid start on Ruby on Rails web development with this book by renowned teacher and author Daniel Kehoe.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Debugging Ruby: Debugging Ruby code & Rails applications by example',
             author: 'Ryan Bigg',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'https://leanpub.com/debuggingruby',
             description: '"The ability to debug code is arguably more important than the ability to write code." I\'ve been helping people debug Ruby code for years now in the #rubyonrails IRC channel on Freenode, Stack Overflow and as a full-time job. Debugging Ruby is a book containing examples of common debugging pitfalls with Ruby code and Rails applications, using cases that I\'ve seen in my day-to-day work. Starting off with some simple Ruby examples and moving into more and more complex territory, this book will educate you in the ways of debugging Ruby code efficiently and what to do in the case of when things go wrong.')
             book.tag_list = 'debugging'
book.save!

 book = Book.new(
             title: 'Multitenancy with Rails',
             author: 'Ryan Bigg',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'https://leanpub.com/multi-tenancy-rails',
             description: 'This book will teach you super neat tricks about Rails and PostgreSQL while you build a multi-tenanted Ruby on Rails application. What\'s a multi-tenanted application? Think of applications like GitHub, where each user or organisation can have their own separate area within the application. Or how about Heroku where each user has their own separate app group. Those are multi-tenanted applications. In this book we\'ll be building a multi-tenanted forum application using some seriously good best practices, working with tools and processes like Behaviour-Driven Development, and PostgreSQL schemas. I\'ll also cover the normal methodology of scoping using foreign keys in a table. That\'s just the first four chapters. In the 5th chapter, the book covers adding subscriptions to an application using the Braintree gateway. A subscription system is typically tough to get right, but with Braintree it\'s a breeze. This book will show you how. In the 6th chapter, we polish up all the rough edges and finish our application.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Beginning Ruby: From Novice to Professional',
             author: 'Peter Cooper',
             publisher: 'apress',
             published: true,
             year: 2009,
             language: 'en',
             link: 'http://www.apress.com/9781430223634/',
             description: 'Based on the bestselling first edition, Beginning Ruby: From Novice to Professional, Second Edition is the leading guide for every type of reader who wants to learn Ruby from the ground up. The new edition of this book provides the same excellent introduction to Ruby as the first edition plus updates for the newest version of Ruby, including the addition of the Sinatra and Ramaze web application frameworks and a chapter on GUI development so developers can take advantage of these new trends. Beginning Ruby starts by explaining the principles behind object-oriented programming and within a few chapters builds toward creating a full Ruby application. By the end of the book, in addition to in-depth knowledge of Ruby, you\'ll also have basic understanding of many ancillary technologies such as SQL, XML, web frameworks, and networking.Introduces readers to the Ruby programming language. Takes readers from basic programming skills to web development with topics like Ruby-based frameworks and GUI programming. Covers many ancillary technologies in order to provide a broader picture (e.g., databases, XML, network daemons).')
             book.tag_list = 'ruby'
book.save!

 book = Book.new(
             title: 'Ruby Quick Syntax Reference',
             author: 'Matt Clements',
             publisher: 'apress',
             published: true,
             year: 2014,
             language: 'en',
             link: 'http://www.apress.com/9781430265689',
             description: 'Ruby Quick Syntax Reference is a condensed code and syntax reference to the Ruby scripting language. It presents the essential Ruby syntax in a well-organized format that can be used as a handy reference. You won\'t find any technical jargon, bloated samples, drawn out history lessons, or witty stories in this book. What you will find is a language reference that is concise, to the point and highly accessible. The book is packed with useful information and is a must-have for any Ruby programmer. In Ruby Quick Syntax Reference, you will find: A concise reference to the Ruby language syntax. Short, simple, and focused code examples. A well laid out table of contents and a comprehensive index, allowing easy review.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Ruby on Rails for PHP and Java Developers',
             author: 'Deepak Vohra',
             publisher: 'apress',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.apress.com/9783540731443',
             description: 'The web framework Ruby on Rails for developing database based web applications provides a Model-View-Controller framework. The required web server WEBrick is included with Ruby on Rails. The framework is configured with the MySQL database by default, but may be configured with another database. The book covers developing web applications with Ruby on Rails. Technologies discussed include Ajax, directory services, and web services. A comparison is made with PHP, the most commonly used scripting language for developing web applications. ')
             book.tag_list = 'Rails, PHP'
book.save!

 book = Book.new(
             title: 'Practical Ruby Projects',
             author: 'Topher Cyll',
             publisher: 'apress',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.apress.com/9781590599112',
             description: 'Want to take Ruby to the limit? Looking for new, powerful, and creative ideas that will take Ruby beyond Rails and web programming? If you’re comfortable with Ruby, you’ll be itching to go further—apply Practical Ruby Projects: Ideas for the Eclectic Programmer and become a master of advanced Ruby techniques. Rubyist Topher Cyll brings several imaginative projects to this book, ranging from making generative music, animations, and turn–based games to implementing simulations, algorithms, and even an implementation of Lisp! Art, music, theory, and games—this book has it all. Best of all, it’s all done with Ruby. Each chapter, in addition to making you say “Cool—I hadn’t thought of that before,” looks at solving tricky development problems, enforces best practices, and encourages creative thinking. You’ll be building your own exciting, imaginative ruby projects in no time. Create imaginative and innovative Ruby programming projects. Learn how to solve tricky development problems, be guided by best practices, and be inspired to think creatively. Don’t waste time on the basics—it’s assumed you know the fundamentals of Ruby already.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Practical Ruby Gems',
             author: 'David Berube',
             publisher: 'apress',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.apress.com/9781590598115',
             description: 'Practical Ruby Gems is a comprehensive guide to utilizing and creating Ruby Gems—ready-made Ruby code modules that can be easily added to Ruby and Rails projects. This book is ideal for Ruby programmers as well as web developers who use Rails and wish to extend the functionality of their projects. You\'ll get a prime selection of 34 of the best and most useful Gems, which makes up the core of this book. Each of these also comes complete with actual use cases and code examples that you can use immediately in your own projects.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Practical Reporting with Ruby and Rails',
             author: 'David Berube',
             publisher: 'apress',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://www.apress.com/9781590599334',
             description: 'Business intelligence and real–time reporting mechanisms play a major role in any of today’s forward–looking business plans. With many of these solutions being moved to the Web, the popular Rails framework and its underlying Ruby language are playing a major role alongside web services in building the reporting solutions of tomorrow. Practical Reporting with Ruby and Rails is the first book to comprehensively introduce this popular framework, guiding readers through a wide–ranging array of features. Note this isn’t a staid guide to generating traditional reports, but rather it shows you how the Ruby language and Rails framework can create truly compelling reporting services by plugging into popular third-party applications and services such as Google AdWords, UPS.com, iTunes, and SalesForce.com.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Practical Ruby for System Administration',
             author: 'André Ben Hamou',
             publisher: 'apress',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.apress.com/9781590598214',
             description: '')
             book.tag_list = 'scripting'
book.save!

 book = Book.new(
             title: 'NetBeans Ruby and Rails IDE with JRuby',
             author: 'Chris Kutler , Brian Leonard',
             publisher: 'apress',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://www.apress.com/9781430216360',
             description: 'Ruby has set the world on fire, proving itself a serious challenger to Perl and Python in all spheres. In particular, more and more people are discovering that Ruby\'s flexibility, superb feature set, and gentle learning curve make it a natural choice for system administration tasks, from the humblest server to the largest enterprise deployment. Within the pages of Practical Ruby for System Administration, you\'ll learn the Ruby way to construct files, tap into clouds of data, build domain-specific languages, perform network traffic analysis, and more. Based on author André Ben Hamou\'s own experiences working as a system administrator, this book will help you pick up practical tips on Ruby coding style, learn how to analyze and improve script performance, and make use of no-nonsense advice on scripting workflow, including testing and documentation. Above all, you\'ll come to appreciate the sheer power of Ruby and the hundreds of benefits it offers for system administration. This book places equal emphasis on fundamental Ruby concepts as well as practical how-tos. It uses examples from other languages to ease the transition to Ruby. The book is concise, entertaining, and informative—unlike many books aimed at system administrators, which can be overly long and stodgy.')
             book.tag_list = 'Rails, JRuby'
book.save!

 book = Book.new(
             title: 'Beginning Ruby on Rails E-Commerce',
             author: 'Jarkko Laine , Christian Hellsten',
             publisher: 'apress',
             published: true,
             year: 2006,
             language: 'en',
             link: 'http://www.apress.com/9781590597361',
             description: 'Beginning Ruby on Rails E-Commerce: From Novice to Professional is the first book of its kind to guide you through producing e-commerce applications with Rails, the stacked web framework taking the world by storm. The book dives right into the process of creating a production-level web application using agile methodologies and test-driven development combined with Rails best practices. You’ll take advantage of the latest crop of Rails plug-ins and helpers that will radically improve your programming schedule. You’ll also create a real application step-by-step, plus the book is driven by real-world cases throughout. You will begin by learning how to install Rails and quickly create a product catalog interfaced with your choice of database technologies. Then you’ll discover how to build modern, Ajax-powered shopping carts and add useful features like customer feedback modules. Next you’ll learn how to integrate your application with open source packages like the Ferret full-text search engine, and how to interface with back-end electronic payment systems. You’ll also learn how to make your application work flawlessly with existing production systems using web services, and then ultimately deploy and tune your application for production use.')
             book.tag_list = 'Rails, ecommerce'
book.save!

 book = Book.new(
             title: 'Rails Solutions',
             author: 'Justin Williams',
             publisher: 'apress',
             published: true,
             year: 2007,
             language: 'en',
             link: 'http://www.apress.com/9781590597521',
             description: 'If you\'re a web designer or developer who thinks that the coding involved in developing dynamic web applications is too difficult, think again. This book, and the framework it covers, is the perfect solution to your needs. Ruby on Rails provides an easy-to-use method for quickly developing web applications, simplifying potentially complicated subjects such as web architecture, JavaScript, and SQL/database creation. The simplicity of Rails belies its power, though—this technology is used by major companies such as 37Signals and Google. The book provides an introduction to Ruby on Rails with the web designer in mind. Instead of focusing on the intricate syntax of each method, the book focuses on the tasks youll want to perform on your website and then walks you through how to implement that functionality with Rails. Design and usability are kept in mind throughout, ensuring that your site both looks and works great. The book begins by covering how to set up your computer as a Rails development environment (including the MySQL database) and then follows with an introduction to Ruby and the basics of the Rails framework. Next, you are taken through several practical examples that work together to build up a complete modern web application, covering essential and useful website features such as user login, adding and editing data, data validation, image uploading, and much more. The book even covers more-advanced Rails topics such as the Rails test suite, plug-ins and components, debugging techniques, and deploying your web applications using Capistrano.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Beginning Rails 4',
             author: 'Adam Gamble , Cloves Carneiro Jr , Rida Al Barazi',
             publisher: 'apress',
             published: true,
             year: 2013,
             language: 'en',
             link: 'http://www.apress.com/9781430260349',
             description: 'Beginning Rails 4 is a book that will guide you from never having programmed with Ruby, to having a Rails application built and deployed to the web. You’ll learn how to combine all the components of Rails to develop your own web applications. You will use test driven development to make sure your application works exactly like you expect. You will learn how to use Git for source control and best practice techniques to create applications like a pro. Essential, and often-missed, information on testing and learning to program with Ruby are also covered. This book is well suited for someone with little to no Ruby or Rails experience, or possibly even someone with no experience developing web applications at all. Beginning Rails 4 does assume a basic familiarity with the web and typical web terms, but doesn’t require you to be an expert of these. This book will springboard your journey into web application development, and show you how much fun building web applications with Ruby on Rails can be.Learn to create Rails applications from the bottom up. Learn the basics of the Ruby programming language. Completely updated for Rails 4, including new information on turbo links, etc. Beginning Rails 4 gently guides you through designing your application, writing tests for the application, and then writing the code to make your application work as expected.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Beginning Rails 3',
             author: 'Rida Al Barazi , Cloves Carneiro Jr , Cloves Carneiro',
             publisher: 'apress',
             published: true,
             year: 2010,
             language: 'en',
             link: 'http://www.apress.com/9781430224334',
             description: 'Beginning Rails 3 is the practical starting point for anyone wanting to learn how to build dynamic web applications using the Rails framework for Ruby. You\'ll learn how all of the components of Rails fit together and how you can leverage them to create sophisticated web applications with less code and more joy. This book is particularly well suited to those with little or no experience with web application development, or who have some experience but are new to Rails. Beginning Rails 3 assumes basic familiarity with web terms and technologies, but doesn\'t require you to be an expert. Rather than delving into the arcane details of Rails, the focus is on the aspects of the framework that will become your pick, shovel, and axe. Part history lesson, part introduction to object-oriented programming, and part dissertation on open source software, this title doesn\'t just explain how to do something in Rails, it explains why. Learn to create Rails web applications from scratch. Includes a gentle introduction to the Ruby programming language. Completely updated to include the features of Rails 3.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Practical REST on Rails 2 Projects',
             author: 'Ben Scofield',
             publisher: 'apress',
             published: true,
             year: 2008,
             language: 'en',
             link: 'http://www.apress.com/9781590599945',
             description: 'Practical REST on Rails 2 Projects is a guide to joining the burgeoning world of open web applications. It argues that opening up your application can provide significant benefits and involves you in the entire process—from setting up your application, to creating clients for it, to handling success and all its attendant problems. This book is the essential resource for anyone who wants to make their web application a full participant in the new Internet. This book is intended for intermediate–to–advanced Rails developers—people who use Rails regularly for sites and applications more complicated than the prototypical roll–your–own blog. In particular, it’s targeted at Rails developers who want to be good Web 2.0 citizens—sharing the functionality of their app with other sites to the betterment of everyone. Application projects include iPhone, Facebook, and REST for the enterprise.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Practical Microservices with Ruby',
             author: 'Gosha Arinich',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'http://practicalmicroservices.com/',
             description: '')
             book.tag_list = 'Rails, services'
book.save!

 book = Book.new(
             title: 'API\'S ON RAILS',
             author: 'Abraham Kuri Vargas',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'http://apionrails.icalialabs.com/book',
             description: 'The best way to build a REST API using Rails.')
             book.tag_list = 'API'
book.save!

 book = Book.new(
             title: 'Geocoding on Rails: Your Map to Geocoding Rails Applications',
             author: 'Josh Clayton, Laila Winner',
             publisher: 'thoughtbot',
             published: true,
             year: 2013,
             language: 'en',
             link: 'https://learn.thoughtbot.com/products/22-geocoding-on-rails',
             description: 'Your Map to Geocoding Rails Applications. If you’ve worked on a web application in the past decade, there’s a good chance you’ve written mapping or geocoding functionality. We’ve worked on numerous Rails applications that geocode certain pieces of data; each time, we had to remind ourselves which Ruby gems to use for searching, how to retrieve coordinate information from the browser, and the correct way to write tests for the behavior we wanted to add. This 60 page book solves these problems by presenting a comprehensive, up-to-date guide to building mapping applications with Ruby on Rails.')
             book.tag_list = 'Rails, geocoding'
book.save!

 book = Book.new(
             title: 'Effective Ruby: 48 Specific Ways to Write Better Ruby',
             author: 'Peter J. Jones',
             publisher: 'Addison-Wesley Professional',
             published: true,
             year: 2014,
             language: 'en',
             link: 'http://www.informit.com/store/effective-ruby-48-specific-ways-to-write-better-ruby-9780133846973',
             description: 'If you’re an experienced Ruby programmer, Effective Ruby will help you harness Ruby’s full power to write more robust, efficient, maintainable, and well-performing code. Drawing on nearly a decade of Ruby experience, Peter J. Jones brings together 48 Ruby best practices, expert tips, and shortcuts—all supported by realistic code examples. Jones offers practical advice for each major area of Ruby development, from modules to memory to metaprogramming. Throughout, he uncovers little-known idioms, quirks, pitfalls, and intricacies that powerfully impact code behavior and performance. Each item contains specific, actionable, clearly organized guidelines; careful advice; detailed technical arguments; and illuminating code examples. When multiple options exist, Jones shows you how to choose the one that will work best in your situation. Effective Ruby will help you systematically improve your code—not by blindly following rules, but by thoroughly understanding Ruby programming techniques. Key features of this concise guide include:    How to avoid pitfalls associated with Ruby’s sometimes surprising idiosyncrasies. What you should know about inheritance hierarchies to successfully use Rails (and other large frameworks). How to use misunderstood methods to do amazingly useful things with collections. Better ways to use exceptions to improve code reliability. Powerful metaprogramming approaches (and techniques to avoid). Practical, efficient testing solutions, including MiniTest Unit and Spec Testing. How to reliably manage RubyGem dependencies. How to make the most of Ruby’s memory management and profiling tools. How to improve code efficiency by understanding the Ruby interpreter’s internals.')
             book.tag_list = 'ruby'
book.save!

 book = Book.new(
             title: 'Ruby Wizardry: An Introduction to Programming for Kids',
             author: 'Eric Weinstein',
             publisher: 'No Starch Press',
             published: true,
             year: 2014,
             language: 'en',
             link: 'http://www.nostarch.com/rubywizardry',
             description: 'The Ruby programming language is perfect for beginners: easy to learn, powerful, and fun to use! But wouldn\'t it be more fun if you were learning with the help of some wizards and dragons? Ruby Wizardry is a playful, illustrated tale that will teach you how to program in Ruby by taking you on a fantastical journey. As you follow the adventures of young heroes Ruben and Scarlet, you’ll learn real programming skills, like how to: Use fundamental concepts like variables, symbols, arrays, and strings. Work with Ruby hashes to create a programmable breakfast menu. Control program flow with loops and conditionals to help the Royal Plumber. Test your wild and crazy ideas in IRB and save your programs as scripts. Create a class of mini-wizards, each with their own superpower!  Organize and reuse your code with methods and lists. Write your own amazing interactive stories using Ruby. Along the way, you’ll meet colorful characters from around the kingdom, like the hacker Queen, the Off-White Knight, and Wherefore the minstrel. Ruby Wizardry will have you (or your little wizard) hooked on programming in no time.')
             book.tag_list = 'children'
book.save!

 book = Book.new(
             title: 'Build your own Rails server',
             author: 'Michael Trojanek',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'http://www.relativkreativ.at/ebook',
             description: 'A carefully handcrafted, DRM-free PDF with about 60 pages of detailed instructions on how to: build a virtual machine from scratch; get a Rails application up and running; deploy a new release. Focused on making each step easily comprehensible and ensuring that you truly understand the process.')
             book.tag_list = 'deployment'
book.save!

 book = Book.new(
             title: 'Practicing Rails: A Guide to Learning Rails Without Getting Overwhelmed',
             author: 'Justin Weiss',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'http://www.justinweiss.com/book/',
             description: 'In this ebook, you’ll learn: How to start your own Rails apps today, and learn as you build them. The best ways to understand the most about Rails in the tiny amount of free time you have. When to pay attention to new gems, libraries, and programming techniques, and when you can ignore them. What error messages mean, where they come from, and how to fix them on your own. Simple processes you can follow to build even the largest features and apps. And most importantly, Practicing Rails will keep you motivated and on the right track to finally mastering Rails.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Fearless Rails Deployment',
             author: 'Zach Campbell',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://railsdeploymentbook.com/',
             description: 'Fearless Rails Deployment is a brand new book released in August 2014 to help you deploy Rails applications better using awesome tools like Chef, RVM, Nginx, Unicorn, and Vagrant.')
             book.tag_list = 'deployment, Rails'
book.save!

 book = Book.new(
             title: 'Web Application Testing in Ruby',
             author: 'Željko Filipin',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/watirbook',
             description: 'Web Application Testing in Ruby (with Selenium and friends) is a book on, well, web application testing in Ruby (Watir). It is not finished. We are working on it. Book source code is at github.com/watir/watirbook. You can discuss the book at groups.google.com/group/watirbook. More information about the book is available at filipin.eu/tag/watirbook. The book is available as PDF, EPUB (iPad, iPhone, iPod) and MOBI (Kindle) file at leanpub.com/watirbook. HTML version of the book is at leanpub.com/watirbook/read. Suggested price is $9.99, but you can get the book for free! Click Buy the ebook now! button and move the You pay slider to the left hand side until the amount goes down to $0.00. Of course, you can move the slider to the right hand side and pay more for the book. All money goes to the Watir project. Money that book readers have donated to the Watir project == $1,523.97 (Updated 2014-01-25. For more stats see stats page.)')
             book.tag_list = 'testing'
book.save!

 book = Book.new(
             title: 'Selenium Recipes in Ruby',
             author: 'Zhimin Zhan',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/selenium_recipes',
             description: 'Selenium 2.0, aka Selenium WebDriver, is a popular browser automation framework. Testers or developers with Selenium skills are in high demand.   It is easy to get started with Selenium, but do you use effectively for testing real-world test scenarios? Such as data driving test from an Excel spreadsheet and handling pop up dialogs. Selenium Recipes will show you solutions to your problems from the experts who have already solved them. All recipe test scripts  (100+ in Ruby language) are ready-to-run, i.e., I created the target web pages and test sites, so that you can simply find the recipes and run, in a matter of seconds. Owning this book is like having a test automation coach sitting next to you.')
             book.tag_list = 'testing'
book.save!

 book = Book.new(
             title: 'Text Processing with Ruby',
             author: 'Rob Miller',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/tpwr',
             description: '')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Ruby on Rails Explained for Front-End Developers',
             author: 'Miles Matthias',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'https://leanpub.com/rorfrontend',
             description: 'Ruby on Rails is an extremely powerful full-stack web application framework, but with its power comes complexity. For front-end developers who simply need to edit HTML, CSS, and JS within a Ruby on Rails application, it can be a daunting task. This book explains what front-end developers needs to know about Ruby on Rails in order to work their mastery within a rails app. By the end of the book, you\'ll be able to: know where to put your html, css, & js within the structure of a ruby on rails app; how rails routes requests to controllers; using views, layouts, and partials in controllers; what the asset pipeline is and how to use it; how controllers and views in rails work together to render a response in the browser; how data is added to views and how to link to the data in your html.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Beginning Core Data with RubyMotion',
             author: 'Stefan Haflidason',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/beginning-core-data-for-ruby-motion',
             description: ' Core Data programming is hard. Apple warn as much in their document Core Data Core Competencies, "Important: Core Data is an advanced technology that is not required for creating simple applications". That doesn\'t sound so severe perhaps but it does contrast greatly with the rest of the developer documentation that seems to suggest that iOS/OSX programming is made easy by all the frameworks and widgets ready for us to customise and use in our applications. Compared to many other Apple frameworks, Core Data seems to be considerably less developer-friendly. Compare it then to the elegance of the RubyMotion framework and the constrast becomes more stark. How are we to define our models, relationships and migrations without the tools that Xcode provides? In this book we set out exactly how you can create Core Data-powered apps using RubyMotion that are ready for the app store, including preparing for the future by learning the ins and outs of schema and data migrations. Below is an outline of the topics we cover: An Overview of the Core Data architecture. Setting up the Core Data stack in RubyMotion. Modelling your data without Xcode. Managing Schema Migrations. Handling Data (Heavyweight) Migrations. Adding undo/redo Management to your app. Tips to get unstuck when faced with common Core Data issues.All sample code is available on GitHub.')
             book.tag_list = 'iOS'
book.save!

 book = Book.new(
             title: 'Constructing Large Scale Enterprise Applications With Ruby And Rails',
             author: 'Karan Singh Garewal',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/largescalesystems',
             description: 'This book discusses the architecture and implementation of  large scale enterprise systems with Ruby and Rails. Architectural patterns for designing  highly concurrent,  scalable and loosely coupled systems are discussed. A Bitcoin Exchange using these patterns is implemented.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'Functional Programming in Ruby',
             author: 'Koen Handekyn',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/functionalprogramminginruby',
             description: 'This book teaches you not about what functional programming is. The target of this book is to help the reader to learn to think differently. Learning to program with a functional style will require you to do some rewiring in the brain. Instead this book helps you to un-learn some of the patterns you\'ve been so very much used to, and learn new functional programming based patterns that help you build consise and efficient solutions to everyday programming problems. At the same time, it will put things in perspective and put functional programming in context, and discuss the limitations of functional programming in general and in Ruby specificially. The end goal is that this book should help the reader to become a better programmer. A better programmer for me is someone who\'s flexible in approaching the problem domain. I\'m a strong believer that in the near future, it\'s going to be more and more key for a developer to know many programming languages and many programming styles. That\'s also one of the reasons why I think Ruby is a good programming language to learn about functional programming style. It\'s far from perfect. It\'s not at all pure. It has limitations. But it allows you to mix and match different programming styles.')
             book.tag_list = 'functional'
book.save!

 book = Book.new(
             title: 'The Happy Lambda',
             author: 'Arne Brasseur',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/happylambda',
             description: 'Learn all about functional programming in the context of your favorite programming language. Get inspired, expand your thinking and become a better programmer in the process.')
             book.tag_list = 'functional'
book.save!

 book = Book.new(
             title: 'Developing Games With Ruby',
             author: 'Tomas Varaneckas',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/developing-games-with-ruby',
             description: 'Once there was a boy who fell in love with this magical device that could bring things to life inside a glaring screen. He spent endless hours exploring imaginary worlds, fighting strange creatures, shooting pixelated spaceships, racing boxy cars. The boy kept pondering. "How is this made? I want to create my own worlds...". Then he discovered programming. "I can finally do it!" - he thought. And he tried. And failed. Then he tried harder. He failed again and again. He was too naive to realize that those worlds he was trying to create were too sophisticated, and his knowledge was too limited. He gave up creating those worlds. What he didn\'t give up is writing code for this magical device. He realized he isn\'t smart enough to create worlds, yet he found out he could create simpler things like small applications - web, desktop, server side or whatnot. Few years later he found himself getting paid to make those. Applications got increasingly bigger, they spanned across multiple servers, integrated with each other, became pats of huge infrastructures. The boy, now a grown man, was all into it. It was fun and challenging enough to spend over 10000 hours learning and building what others wanted him to build. Some of these things were useful, some where boring and pointless. Some were never finished. There were things he was proud of, there were others that he wouldn\'t want to talk about, nonetheless everything he built made him a better builder. Yet he never found the time, courage or reason to build what he really wanted to build since he was a little boy - his own worlds. Until one day he realized that no one can stop him from following his dream. He felt that equipped with his current knowledge and experience he will be able to learn to create worlds of his own. And he went for it. This boy must live in many software developers, who dream about creating games, but instead sell their software craftsmanship skills to those who need something else. This boy is me, and you. And it\'s time to set him free. Welcome to the world of game development that was waiting for you all these years.')
             book.tag_list = 'game'
book.save!

 book = Book.new(
             title: 'Learn Ruby Programming by Examples',
             author: 'Zhimin Zhan, Courtney Zhan',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/learn_ruby_programming_by_examples_en',
             description: 'On December 8, 2013, US President Barack Obama "asked every American to give it a shot to learn to code" (watch it here), kicking off the Hour of Code campaign for Computer Science Education Week 2013. "Learning these skills isn\'t just important for your future, it\'s important for our country\'s future," President Obama said. The message is clear: coding (aka. programming) is an important skill for this Information Age, and many will agree.  Some might wonder: there are many "how to program" books, why another one? A typical how-to-program book will go through the programming concepts, syntax and followed by demonstrations with simple examples. I have read dozens of them (for different programming languages) and taught this way at universities. It was not an effective approach. It is more like a teacher dumping knowledge upon students. I believe a better way is to engage students in doing carefully selected programming exercises and guiding them solving interesting and useful computer programs. New programming concepts are introduced gradually.  I put this into practices by teaching my 12-year old daughter Courtney. This book is the outcome of the journey.')
             book.tag_list = 'ruby'
book.save!

 book = Book.new(
             title: 'Ruby Kin',
             author: 'Doug Wright',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/rubykin',
             description: 'Want to learn programming but don\'t know where to start? This book will help demistify the basic principles in every programming language. No computer science degree required.Written for kids and adults, Ruby Kin will show you how programming is nothing more than instructions you give your computer.')
             book.tag_list = 'children'
book.save!

 book = Book.new(
             title: 'AngularJS + Rails',
             author: 'Jonathan Birkholz, Jesse Wolgamott',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/angularails',
             description: 'AngularJS examples seem ok, but how do you fit it into Rails? You’ve seen the online AngularJS examples that get you far enough to type in a box and see it on the screen, but it’s not enough to build something REAL. The blog posts, js-fiddles, and walkthroughs don’t get you connected to Rails, they don’t help with data validation, and they don’t help at all trying to get Angular working with the Asset Pipeline. You’re left wondering where to start, and doubting if you should use Rails at all. You need more than "Hello World." To get productive in AngularJS, you need real world examples extracted from real world applications. You can easily pick up the "Hello World" on $scope examples -- you need to dive deep in order to use AngularJS in production. What about authentication? deployment? Blog posts and even books about AngularJS leave you wondering where to go for problems that were solved by Rails years ago. But you’re left frustrated and lost when it comes to these “simple” services in Angular + Rails. You’ll be able to create an AngularJS + Rails app with confidence. With AngularJS+Rails, you’ll have the knowledge, experience, and reference materials to create apps with AngularJS. You\'ll be able to use Rails as an API, or integrate AngularJS with traditional Rails apps. We cover routing+templates for your large applications, and how to keep things simple when you’re enhancing a form or adding real-time updates to a feed. You\'ll know how to hook in Devise with AngularJS, how to deploy, and how to make sure your users can press refresh.')
             book.tag_list = 'AngularJS'
book.save!

 book = Book.new(
             title: 'Riding Rails with AngularJS',
             author: 'Ari Lerner',
             publisher: 'Self Published',
             published: true,
             year: 2013,
             language: 'en',
             link: 'https://leanpub.com/angularjs-rails',
             description: 'We are selling this book along with the code at: http://www.fullstack.io/edu/angular/rails/. The book itself is available here on leanpub as well.')
             book.tag_list = 'AngularJS'
book.save!

 book = Book.new(
             title: 'Deploy Rails BlueBook 2014 Edition',
             author: 'John B. Wyatt IV',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/deployrailsbluebook',
             description: 'Have you ever wanted know how to automate your tedious server setup? Sick of dealing with bad tutorials on the web? Sick of looking up books that was intended for versions of Chef and Puppet written years ago? Want to deploy from Github without sharing your password? Want to use PostgreSQL, but the instructions make no sense? Grab this book today and get a fully tested and documented, modern Rails stack in an easy to understand format. Spend less time looking up error messages and more time coding! This book covers Ubuntu 12.04, Ruby 1.9.3, PostgreSQL, Passenger and deploying from Github. The first part covers manual setup and the second shows how to automate the manual steps with Chef. A few choice features that will save you alot of time and frustration. Using Knife solo with Chef to deploy by IP address, without needing a separate Chef server (or Puppetmaster). Fetching your Rails app from Github.- Using the figaro gem to keep your passwords and other information outside your git repo. Setting up PostgreSQL on a local machine manually and later automating. A guide to using Vagrant to setup a local development machine with a few simple commands.- Automating everything with Chef, as it should be! Who is this book for?This book is for those who have written a Rails app and have never deployed to a public server before. Or perhaps you have deployed and you want to know how to automate it. I cover Rails 3.2 with Ruby 1.9.3, Ubuntu 12.04 64 bit, PostgreSQL and Nginx with Passenger. I assume some background with Ubuntu and Rails.')
             book.tag_list = 'deployment'
book.save!

 book = Book.new(
             title: 'Rails Crash Course',
             author: 'Anthony Lewis',
             publisher: 'No Starch Press',
             published: true,
             year: 2014,
             language: 'en',
             link: 'http://www.nostarch.com/railscrashcourse',
             description: 'Rails is a robust, flexible development platform that lets you build complex websites quickly. Major websites like GitHub, Hulu, and Twitter have run Rails under the hood, and if you know just enough HTML and CSS to be dangerous, Rails Crash Course will teach you to harness Rails for your own projects and create web applications that are fast, stable, and secure. In Part I, you’ll learn Ruby and Rails fundamentals and then dive straight into models, controllers, views, and deployment. As you work through the basics, you’ll learn how to: Craft persistent models with Active Record. Build view templates with Embedded Ruby. Use Git to roll back to previous versions of your code base. Deploy applications to Heroku. In Part II, you’ll take your skills to the next level as you build a social networking app with more advanced Ruby tools, such as modules and metaprogramming, and advanced data modeling techniques within Rails’s Active Record. You’ll learn how to: Implement an authentication system to identify authorized users. Write your own automated tests and refactor your code with confidence. Maximize performance with the asset pipeline and turbolinks. Secure your app against SQL injection and cross-site scripting. Set up a server and deploy applications with Capistrano. Each chapter is packed with hands-on examples and exercises to reinforce what you’ve learned. Whether you’re completely new to Ruby or you’ve been mucking around for a bit, Rails Crash Course will take you from the basics to shipping your first Rails application, fast.')
             book.tag_list = 'Rais'
book.save!

 book = Book.new(
             title: 'Trailblazer A New Architecture For Rails',
             author: 'Nick Sutterer',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'https://leanpub.com/trailblazer',
             description: 'Trailblazer is a thin layer on top of Rails and brings a high-level application architecture, decent encapsulation and a new code structure. Here are the main features. Logicless models solely focusing on persistence. Skinny controllers working as endpoints, only. Domain objects for your business logic, known as operations. Form objects for deserialisation and validation. View models to introduce a widget architecture. Representers that parse and render documents for APIs. Twin objects for decorating models. This book takes you for an adventure trip from Rails\' code jungle to Trailblazer beach. We\'re building a complete Rails application using all of Trailblazer\'s goodies while exploiting the Rails Way where it helps us. What we build here is not just a simple 5-minutes blog, but a full-blown commenting application with complex forms, composed views, signed-in users, moderators, authentication rules, a document-based JSON API, and more. We have a strong focus on testing, object design and structuring the code using Trailblazer\'s concept-oriented framework.')
             book.tag_list = 'Rails'
book.save!

 book = Book.new(
             title: 'The Ruby Closures Book',
             author: 'Benjamin Tan',
             publisher: 'Self Published',
             published: true,
             year: 2015,
             language: 'en',
             link: 'http://rubyclosur.es/',
             description: 'What you\'ll learn from this book? What are closures? Why and how are closures useful? The differences between lambdas and procs. Ruby patterns using blocks. Real-world examples of closures. Implement your own testing framework, lazy enumerables and DSLs. And much more ...')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'The Rake Field Manual',
             author: 'Avdi Grimm',
             publisher: 'Self Published',
             published: true,
             year: 2014,
             language: 'en',
             link: 'http://www.rakefieldmanual.com/',
             description: 'The Rake Field Manual is a book-in-progress about the Rake build tool, by Avdi Grimm.')
             book.tag_list = ''
book.save!

 book = Book.new(
             title: 'Ruby DSL Handbook',
             author: 'Jim Gay',
             publisher: 'Self Published',
             published: true,
             year: 2015,
             language: 'en',
             link: 'http://clean-ruby.com/dsl',
             description: 'You\'ve read about building Domain Specific Languages with Ruby but it can be hard to make sense not only the metaprogramming but the myriad of opinions and approaches. How do you get started and how can you make it all work? It can make your head spin when you dive in to try it. You know writing DSLs in Ruby is powerful and flexible, but aren\'t sure exactly how to get started. The worst part of attempting to write a DSL is worrying that you\'ll get stuck without knowing where to turn. What\'s the difference between class_eval and instance_eval? And how do you know when to use them? Taking control of your project with custom DSLs would make your code so much more expressive. Learn to make code that expresses your team\'s shared understanding of a business problem. Projects like Ruby on Rails provide sensible DSLs for managing relationships between models in your system. The Sinatra framework gives you a DSL to provide affordances in your code so that web requests are handled the way you expect. Chef was created as a DSL for managing server configurations. You can model your project\'s needs with you own DSL too. From the author of Clean Ruby, Ruby DSL Handbook gives you the knowledge you need to build the right tool for your business domain. Ruby DSL Handbook will give you the knowledge and experience to create expressive programs. Use business terms in code that make sense to you and your team; read Ruby DSL Handbook and bend your code to demands of your business.')
             book.tag_list = 'DSL'
book.save!

Book.update_all(category_id: Category.first)
