# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comment_en do
    body "Factory Girl advise thise book for everyone"
    language "en"
    book_id 1
    user_id 1
  end

  factory :comment_ru do
    body "Factory Girl рекомендует данную книгу для прочтения"
    language "ru"
    book_id 1
    user_id 1
  end
end
