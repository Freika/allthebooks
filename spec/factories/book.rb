FactoryGirl.define do
  factory :book do
    sequence(:title) { |n| "Easy Active Record #{n}" }
    author "W. Jason Gilmore "
    publisher "Self Published "
    year 2013
    link "http://www.jstorimer.com/products/working-with-ruby-threads"
    description "Easy Active Record for Rails Developers is the ultimate guide to mastering building database-driven Rails applications using the powerful Active Record ORM Framework. By following along with the development of a fun, web application for vintage arcade game aficionados, you’ll learn how to: Choose and implement the belongs_to, has_one, has_many, and has_and_belongs_to_many model associations; Properly validate, filter and manipulate your application’s model data; Manage model schemas using Active Record’s powerful migrations tool; Create and integrate complex web forms; Deftly traverse associations using includes and joins; Monitor, debug and optimize your queries; Integrate user registration and authentication features using the Devise gem."
    language "en"
    views 0
    tag_list "Rails"
    category { create(:category) }

    factory :published_book do
      published true
    end

    factory :unpublished_book do
      published false
    end
  end
end
