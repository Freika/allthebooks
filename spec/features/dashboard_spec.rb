require 'rails_helper'

feature 'Dashboard' do
  given(:admin) { create(:admin) }

  scenario 'can be visited by authenticated admin' do
    visit rails_admin_path

    fill_in 'Email', with: admin.email
    fill_in 'Password', with: admin.password

    click_on 'Log in'

    expect(page).to have_content 'All the Books Dashboard'
  end

  scenario 'cannot be visited by guest' do
    visit rails_admin_path

    expect(page).to have_content 'Forgot your password?'
  end

end
