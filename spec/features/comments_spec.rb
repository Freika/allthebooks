require 'rails_helper'

feature 'access top page' do
  given(:book) { create(:book) }

  scenario 'can sign in user with Twitter account' do
    visit '/'
    click_on 'Войти через Twitter'
    visit category_book_path(book.category, book.slug)

    within("#russian") do
      fill_in 'comment_body', with: 'Comment was added'
      click_on 'Оставить комментарий'
    end

    expect(page).to have_content 'Example User: Comment'
    expect(page).to have_content "Вы уже оставили комментарий к данной книге"

    within('#english') do
      expect(page).not_to have_content "Comment was added"
    end
  end
end

describe "user comment" do

end
