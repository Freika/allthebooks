require 'rails_helper'

feature 'In tags' do
  given!(:book) { create(:published_book) }
  given!(:another_book) { create(:published_book, title: 'Another book.', tag_list: ["Rails", "Ruby"]) }

  scenario 'current tag_path should return all the tagged books' do
    visit tags_path("Rails")
    expect(page).to have_content book.title
    expect(page).to have_content another_book.title
  end

  scenario 'tag_path shouldnt return untagged books' do
    visit tags_path("Ruby")
    expect(page).to have_no_content book.title
  end

  scenario 'tag_path should return tagged books that matches all given tags' do
    visit tags_path("Rails&Ruby")
    expect(page).to have_no_content book.title
    expect(page).to have_content another_book.title
  end

  scenario 'category page has correck tag links' do
    visit category_path(book.category)
    click_link 'Rails'
    expect(page).to have_content book.title
  end
end
