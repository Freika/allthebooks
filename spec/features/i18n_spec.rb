require 'rails_helper'

feature 'I18n' do
  before do
    @category = create(:category)
    book1 = Book.create(title: 'The Ruby book', published: true, locale: :en, category: @category)
    book1.update(title: 'Книга по Руби', locale: :ru)
    book2 = Book.create(title: 'The Rails book', published: true, locale: :en, category: @category)
  end

  scenario 'it should return valid model translation' do
    visit change_locale_path(:en)

    visit category_books_path(@category)

    expect(page).to have_content 'The Ruby book'

    visit change_locale_path(:ru)
    visit category_books_path(@category)

    expect(page).to have_content 'Книга по Руби'
    expect(page).to have_content 'The Rails book'
  end

  scenario 'it should return valid translations for static data' do
    visit change_locale_path(:en)
    expect(page).to have_content 'Login via Twitter'

    visit change_locale_path(:ru)
    expect(page).to have_content 'Войти через Twitter'
  end
end
