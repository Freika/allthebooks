require 'rails_helper'

feature 'Friendly URLs' do
  given(:book) { create(:book) }

  scenario 'return corresponding bok' do
    visit category_book_path(book.category, book.slug)
    expect(Book.last.slug).to eq book.slug
    expect(page).to have_content book.description
    expect(page).to have_no_content '404'
  end
end
