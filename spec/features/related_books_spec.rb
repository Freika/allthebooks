require 'rails_helper'

feature 'related books' do
  given!(:book) { create(:published_book) }
  given!(:another_book) { create(:published_book, title: 'Some another rails book.') }

  scenario 'can be seen in books#show action' do
    visit category_book_path(book.category, book.slug)

    expect(page).to have_content another_book.title
  end

end
