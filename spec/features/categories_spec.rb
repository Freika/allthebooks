require 'rails_helper'

feature 'Categories' do
  given!(:category) { create(:category, title: 'Ruby') }
  given!(:first_book) { create(:published_book, title: 'First book', category: category, views: 1) }
  given!(:second_book) { create(:published_book, title: 'Second book', category: category, views: 2) }
  given!(:third_book) { create(:published_book, title: 'Third book', category: category, views: 3) }
  given!(:another_category) { create(:category, title: 'Python') }
  given!(:another_book) { create(:published_book, category: another_category) }

  scenario 'main page contains all categories' do
    visit root_path

    expect(page).to have_content category.title.capitalize
    expect(page).to have_content another_category.title.capitalize
  end

  scenario 'category page contains books belongs to this category' do
    visit category_path(category)

    expect(page).to have_content first_book.title
    expect(page).to_not have_content another_book.title

    visit category_path(another_category)

    expect(page).to_not have_content first_book.title
    expect(page).to have_content another_book.title
  end

  scenario 'books ordered by views' do
    visit root_path

    within first('.top-books .col-sm-3:nth-child(1)') do
      expect(page).to have_content third_book.title
    end

    within first('.top-books .col-sm-3:nth-child(2)') do
      expect(page).to have_content second_book.title
    end

    within first('.top-books .col-sm-3:nth-child(3)') do
      expect(page).to have_content first_book.title
    end
  end

end
