require 'rails_helper'

feature 'access top page' do
  scenario 'can sign in user with Twitter account' do
    visit '/'
    expect(page).to have_content('Войти через Twitter')
    click_on 'Войти через Twitter'

    expect(page).to have_content('Example User')
    expect(page).to have_content('Выход')
  end

  scenario 'can sign in user with Github account' do
    visit '/'
    expect(page).to have_content('Войти через Github')
    click_on 'Войти через Github'

    expect(page).to have_content('Example User')
    expect(page).to have_content('Выход')
  end

  pending 'can handle authentication error' do
    OmniAuth.config.mock_auth[:twitter] = :invalid_credentials
    visit '/'
    expect(page).to have_content('Войти через Twitter')
    click_on 'Войти через Twitter'
    expect(page).to have_content('Authentication failed.')
  end

end
