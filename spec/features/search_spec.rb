require 'rails_helper'

feature 'Search' do
  given!(:book) { create(:published_book) }

  scenario 'it should return valid search results' do
    visit root_path

    fill_in 'search', with: 'Active Record'
    within '.search' do
      click_button ''
    end

    expect(page).to have_content book.title
  end

  scenario 'should return notice when cant find any book' do
    visit root_path

    fill_in 'search', with: 'abrvabr'
    click_button ''
    expect(page).to have_content "There wasn't found any book."
  end
end
