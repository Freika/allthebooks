require 'rails_helper'

feature 'Book' do
  given(:book) { build(:book) }

  scenario 'can be added to review' do
    visit new_book_path

    fill_in I18n.t('books.new.title'),       with: book.title
    fill_in I18n.t('books.new.author'),      with: book.author
    fill_in I18n.t('books.new.year'),        with: book.year
    fill_in I18n.t('books.new.link'),        with: book.link
    fill_in I18n.t('books.new.description'), with: book.description
    fill_in I18n.t('books.new.lang'),        with: book.language
    click_on I18n.t('books.show.create')

    expect(Book.last.title).to eq book.title
    expect(Book.last.description).to eq book.description
    expect(Book.last.published).to eq false
  end

end
