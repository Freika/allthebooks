ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'capybara/rspec'
require "factory_girl_rails"
require "database_cleaner"

Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

  config.after(:each) do
    Rails.application.routes.default_url_options.delete(:locale)
  end

  config.include(OmniauthMacros)
  config.include FactoryGirl::Syntax::Methods
  config.include Rails.application.routes.url_helpers
end

OmniAuth.config.test_mode = true
